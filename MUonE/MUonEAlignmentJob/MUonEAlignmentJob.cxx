#include "MUonEAlignmentJob.h"

#include <fairlogger/Logger.h>


MUonEAlignmentJob::MUonEAlignmentJob()
{
    resetDefaultConfiguration();
}

Bool_t MUonEAlignmentJob::readConfiguration(TString config_name) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    LOG(info) << "";
    LOG(info) << "Reading job configuration: " << config_name.Data();

    YAML::Node job;

    if(!loadConfigFile(config_name, job))
        return false;


    if(job["inputFile"]) {
        m_inputFile = job["inputFile"].as<std::string>();

        if(m_inputFile.size() > 5) {

            auto sbst = m_inputFile.substr(m_inputFile.size() - 5);

            if(0 != sbst.compare(".root") && 0 != sbst.compare(".ROOT"))
                m_inputFile.append(".root");
        } else 
            m_inputFile.append(".root");
    }
    else {
        LOG(error) << "Variable 'inputFile' not set.";
        m_configuredCorrectly = false;
    }

    if(job["detectorConfig"])
        m_detectorConfig = job["detectorConfig"].as<std::string>();
    else {
        LOG(error) << "Variable 'detectorConfig' not set.";
        m_configuredCorrectly = false;
    }


    if(job["numberOfEvents"])
        m_numberOfEvents = job["numberOfEvents"].as<Int_t>();

    if(job["outputFile"]) {
        m_outputFile = job["outputFile"].as<std::string>();
        m_outputFileSet = true;

        if(m_outputFile.size() > 5) {

            auto sbst = m_outputFile.substr(m_outputFile.size() - 5);

            if(0 != sbst.compare(".yaml") && 0 != sbst.compare(".YAML"))
                m_outputFile.append(".yaml");
        } else 
            m_outputFile.append(".yaml");
    }
    else {
        LOG(info) << "Variable 'outputFile' not set, default name will be used.";
        m_outputFileSet = false;
    }        

    if(job["saveOutputInAlignmentDirectory"])
        m_saveOutputInAlignmentDirectory = job["saveOutputInAlignmentDirectory"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveOutputInAlignmentDirectory' not set, by default alignment files will be saved in the alignment directory.";
    }        

    if(job["alignmentConfiguration"]) {

        if(!m_alignmentConfiguration.readConfiguration(job["alignmentConfiguration"]))
            m_configuredCorrectly = false;
    }
    else {

        LOG(info) << "'alignmentConfiguration' section not found. Default settings will be used.";
    }


    if(m_configuredCorrectly)
        LOG(info) << "Job configuration read successfully.";
    else
        LOG(info) << "Job configuration contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}


void MUonEAlignmentJob::logCurrentConfiguration() const {

    LOG(info) << "";
    LOG(info) << "";
    LOG(info) << "Summary of alignment job configuration";

    LOG(info) << "Alignment will be run on pre-generated file: " << m_inputFile << " with detector configuration: " << m_detectorConfig;
    
    m_alignmentConfiguration.logCurrentConfiguration();
}


void MUonEAlignmentJob::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_inputFile = "";
    m_detectorConfig = "";

    m_numberOfEvents = -1;

    m_outputFileSet = false;
    m_outputFile.clear();

    m_saveOutputInAlignmentDirectory = true;

    m_alignmentConfiguration.resetDefaultConfiguration();

}

Bool_t MUonEAlignmentJob::loadConfigFile(TString config_name, YAML::Node& config) {

    if(!config_name.EndsWith(".yaml") && !config_name.EndsWith(".YAML"))
        config_name.Append(".yaml");

    try {

        config = YAML::LoadFile(config_name.Data());
        LOG(info) << "Configuration file found in the path provided.";

    } catch(...) {

        TString jobs_dir = getenv("JOBSPATH");
        jobs_dir.ReplaceAll("//", "/"); //taken from FR examples, not sure if necessary
        if(!jobs_dir.EndsWith("/"))
            jobs_dir.Append("/");  

        TString full_path = jobs_dir + config_name;

        LOG(info) << "Configuration file in the given path not found or failed to open. Trying in JOBSPATH directory (" << full_path << ").";

        try {

            config = YAML::LoadFile(full_path.Data());
            LOG(info) << "File found.";

        } catch(...) {

            LOG(fatal) << "Failed to load job configuration.";
            return false;
        }
    }   

    return true; 
}

ClassImp(MUonEAlignmentJob)