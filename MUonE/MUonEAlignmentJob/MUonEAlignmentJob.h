#ifndef MUONEALIGNMENTJOB_H
#define MUONEALIGNMENTJOB_H

#include <yaml-cpp/yaml.h>
#include "TString.h"
#include <string>

#include "Rtypes.h"

#include "MUonEAlignmentConfiguration.h"

class MUonEAlignmentJob {


public:

    MUonEAlignmentJob();

    Bool_t readConfiguration(TString config_name);
    void logCurrentConfiguration() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}


    std::string inputFile() const {return m_inputFile;}
    std::string detectorConfiguration() const {return m_detectorConfig;}

    Int_t numberOfEvents() const {return m_numberOfEvents;}

    Bool_t outputFileSet() const {return m_outputFileSet;}
    std::string const& outputFile() const {return m_outputFile;}

    Bool_t saveOutputInAlignmentDirectory() const {return m_saveOutputInAlignmentDirectory;}

    MUonEAlignmentConfiguration const& alignmentConfiguration() const {return m_alignmentConfiguration;}


    void resetDefaultConfiguration();

private:

    Bool_t loadConfigFile(TString config_name, YAML::Node& config);

    Bool_t m_configuredCorrectly{false};

    std::string m_inputFile;
    std::string m_detectorConfig;

    Int_t m_numberOfEvents;

    Bool_t m_outputFileSet{false};
    std::string m_outputFile;

    Bool_t m_saveOutputInAlignmentDirectory{true};

    MUonEAlignmentConfiguration m_alignmentConfiguration;


    ClassDef(MUonEAlignmentJob, 2)
};

#endif //MUONEALIGNMENTJOB_H

