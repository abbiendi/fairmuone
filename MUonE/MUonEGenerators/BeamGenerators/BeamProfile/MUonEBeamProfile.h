#ifndef MUONEBEAMPROFILE_H
#define MUONEBEAMPROFILE_H

#include <string>
#include <array>

#include "Rtypes.h"
#include "TFile.h"
#include "TString.h"
#include "TVector3.h"
#include "THnSparse.h"


class MUonEBeamProfile {

public:

    MUonEBeamProfile() = default;
    ~MUonEBeamProfile();

    Bool_t loadBeamProfile(TString profilename);
    void generateBeamKinematics(TVector3& beamPosition, TVector3& beamMomentum, Double_t xOffset = 0, Double_t yOffset = 0);

    Double_t getNominalBeamMomentum() const;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}
    TString filePath() const {return m_filePath;}

private:

    Bool_t m_configuredCorrectly{false};
    TString m_filePath;
    TFile* m_file{nullptr};

    THnSparseD* m_beamProfile{nullptr};
    std::array<Double_t, 5> m_beamParameters;

	ClassDef(MUonEBeamProfile,2)

};

#endif //MUONEBEAMPROFILE_H 

