#ifndef MUONEBEAMGENERATOR_H
#define MUONEBEAMGENERATOR_H

#include "Rtypes.h"
#include "TVector3.h"
#include <string>

#include "MUonEBeamGeneratorConfiguration.h"

class MUonEBeamGenerator {

public:

    MUonEBeamGenerator(std::string name = "MUonEBeamGenerator")
        : m_name(name)
        {}

    virtual Bool_t setConfiguration(const MUonEBeamGeneratorConfiguration* config) = 0;
    virtual void logCurrentConfiguration() const = 0;

    virtual Bool_t generateBeam(Int_t& pdg, Double_t& energy, TVector3& beamPosition, TVector3& beamMomentum) = 0;
    virtual Double_t getNominalBeamEnergy() const = 0;

    virtual void resetDefaultConfiguration() = 0;

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}
    std::string name() const {return m_name;}

protected:

    Bool_t m_configuredCorrectly{false};
    std::string m_name;

    ClassDef(MUonEBeamGenerator, 2)

};

#endif//MUONEBEAMGENERATOR_H

