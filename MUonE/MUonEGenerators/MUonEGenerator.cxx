#include "MUonEGenerator.h"
#include "MUonEStack.h"

#include "TRandom.h"
#include "TMath.h"
#include "TVector3.h"
#include "TVirtualMC.h"

#include <fairlogger/Logger.h>

#include <iostream>

#include "MUonEParticleGunBox.h"
#include "MUonEParticleGunBeamProfile.h"
#include "MUonEVertexGeneratorLOSimple.h"
#include "MUonEVertexGeneratorMesmer.h"




MUonEGenerator::MUonEGenerator()
	: FairGenerator()
	{
		resetDefaultConfiguration();
	}

MUonEGenerator::~MUonEGenerator()
{

	if(m_beamGenerator) {

		delete m_beamGenerator;
		m_beamGenerator = nullptr;
	}

	if(m_vertexGenerator) {

		delete m_vertexGenerator;
		m_vertexGenerator = nullptr;
	}	
}

void MUonEGenerator::Finish() {

	if(m_vertexGenerator)
		m_vertexGenerator->Finalize(eventsCounter(), verticesCounter());
}


Bool_t MUonEGenerator::setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonEGenerationConfiguration const& generationConfig, Int_t seed, Int_t numberOfEvents, std::string outputFile) {

	resetDefaultConfiguration();
	m_detectorConfiguration = detectorConfig;

	m_configuredCorrectly = true;
	m_configuredCorrectly = detectorConfig.configuredCorrectly() & generationConfig.configuredCorrectly();
	LOG(info) << "";
	LOG(info) << "Creating generator object.";

	m_generationConfiguration = generationConfig;

	auto beam_generator_name = generationConfig.beamGenerator();
	auto beam_generator_base_type = generationConfig.beamGeneratorConfiguration()->baseType();
	auto beam_generator_configuration = generationConfig.beamGeneratorConfiguration();

	if(0 == beam_generator_base_type.compare("Box")) {

		auto tmp_ptr = new MUonEParticleGunBox();
		if(!tmp_ptr->setConfiguration(beam_generator_configuration)) {
			m_configuredCorrectly = false;
			delete tmp_ptr;
		}
		else
			m_beamGenerator = tmp_ptr;

	} else if(0 == beam_generator_base_type.compare("BeamProfile")) {

		auto tmp_ptr = new MUonEParticleGunBeamProfile();
		if(!tmp_ptr->setConfiguration(beam_generator_configuration)) {
			m_configuredCorrectly = false;
			delete tmp_ptr;
		}
		else
			m_beamGenerator = tmp_ptr;
	

	} else {

		LOG(error) << "Beam generator " << beam_generator_name << " not known.";
		m_configuredCorrectly = false;
	}


	if(generationConfig.forceInteraction()) {

		auto vertex_generator_name = generationConfig.vertexGenerator();
		auto vertex_generator_base_type = generationConfig.vertexGeneratorConfiguration()->baseType();
		auto vertex_generator_configuration = generationConfig.vertexGeneratorConfiguration();

		if(0 == vertex_generator_base_type.compare("LOSimple")) {

			auto tmp_ptr = new MUonEVertexGeneratorLOSimple();
			if(!tmp_ptr->setConfiguration(vertex_generator_configuration)) {
				m_configuredCorrectly = false;
				delete tmp_ptr;
				tmp_ptr = nullptr;
			}
			else 
				m_vertexGenerator = tmp_ptr;
		
		} else if(0 == vertex_generator_base_type.compare("Mesmer")) {	
		
			auto tmp_ptr = new MUonEVertexGeneratorMesmer();

			if(!tmp_ptr->setConfiguration(vertex_generator_configuration)) {
				m_configuredCorrectly = false;
				delete tmp_ptr;
				tmp_ptr = nullptr;
			}
			else {

				tmp_ptr->setSeed(seed);
				tmp_ptr->setNumberOfEvents(numberOfEvents);	
				tmp_ptr->setOutputFile(outputFile);
				if(nullptr != m_beamGenerator)
					tmp_ptr->setNominalBeamEnergy(m_beamGenerator->getNominalBeamEnergy());
				m_vertexGenerator = tmp_ptr;
			}
		
		} else if(0 == vertex_generator_base_type.compare("ForceCollisionBiasing")) {

			//this is handled by Geant4, we'll make sure vertexGenerator is a nullptr for future checks
			//note that checks below are based on nullptr to not change forceInteraction variable in config
			//and not have to store it internally in the generator class
			m_vertexGenerator = nullptr;


		} else {

			LOG(error) << "Vertex generator " << vertex_generator_name << " not known.";
			m_configuredCorrectly = false;	
		}
	}

	if(nullptr != m_vertexGenerator && !beam_generator_configuration->isEnergyValid()) {

        LOG(error) << "forceInteraction set to true, but beamGenerator doesn't have enough information to provide beam energy.";
        m_configuredCorrectly = false;		
	}

    if(m_configuredCorrectly)
        LOG(info) << "Generator object created successfully.";
    else
        LOG(info) << "Generator configuration contains errors. Please fix them and try again.";
	
	return m_configuredCorrectly;
}


void MUonEGenerator::logCurrentConfiguration() {

	LOG(info) << "";
	LOG(info) << "Generators configuration:";
	LOG(info) << "";
	m_generationConfiguration.logCurrentConfiguration();
	LOG(info) << "";
	m_beamGenerator->logCurrentConfiguration();
	if(m_vertexGenerator) {
		LOG(info) << "";
		m_vertexGenerator->logCurrentConfiguration();
	}
}

void MUonEGenerator::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_detectorConfiguration.resetDefaultConfiguration();
	m_generationConfiguration.resetDefaultConfiguration();

	if(m_beamGenerator) {

		delete m_beamGenerator;
		m_beamGenerator = nullptr;
	}

	if(m_vertexGenerator) {

		delete m_vertexGenerator;
		m_vertexGenerator = nullptr;
	}	

	m_targetMinX = 0;
	m_targetMaxX = 0;
	m_targetMinY = 0;
	m_targetMaxY = 0;
	m_targetMinZ = 0;
	m_targetMaxZ = 0;

}

Bool_t MUonEGenerator::Init() {

	LOG(info) << "Initializing generator.";
	m_eventsCounter = 0;

	if(m_detectorConfiguration.hasTargets() && m_vertexGenerator != nullptr) {

		m_vertexStationIndex = m_generationConfiguration.vertexStationIndex();
		auto stations = m_detectorConfiguration.stations();

		if(m_vertexStationIndex < 0 || m_vertexStationIndex >= stations.size() || !stations[m_vertexStationIndex].hasTarget()) {

			LOG(error) << "Selected vertexStationIndex is out of range or selected station has no target.";
			return false;
		}

		auto target_position = stations[m_vertexStationIndex].targetPosition();
		auto target_config = stations[m_vertexStationIndex].targetConfiguration();
		auto target_width = target_config.width();
		auto target_height = target_config.height();
		auto target_thickness =target_config.thickness();

		m_targetMinX = -0.5 * target_width;
		m_targetMaxX = 0.5 * target_width;
		m_targetMinY =  -0.5 * target_height;
		m_targetMaxY = 0.5 * target_height;
		m_targetMinZ = target_position - 0.5 * target_thickness;
		m_targetMaxZ = target_position + 0.5 * target_thickness;

		return m_vertexGenerator->Init();	
	}

	return true;
}

Bool_t MUonEGenerator::ReadEvent(FairPrimaryGenerator* primGen) {

    Int_t failed_attempts = 0;

    EVENT_GENERATION_START:

	Int_t beam_pdg;
    Double_t beam_energy;
    TVector3 beam_position;
    TVector3 beam_momentum;

	auto addPileup = [this, primGen]() {

		Int_t N = 0;
		do {
			N = gRandom->Poisson(m_generationConfiguration.pileupMean());
		} while(N == 0);


		Int_t beam_pdg;
		Double_t beam_energy;
		TVector3 beam_position;
		TVector3 beam_momentum;

		auto trackStack = dynamic_cast<MUonEStack*>(TVirtualMC::GetMC()->GetStack());

		for(Int_t i = 0; i < N - 1; ++i) {

			if(m_beamGenerator->generateBeam(beam_pdg, beam_energy, beam_position, beam_momentum)) {

				primGen->AddTrack(beam_pdg, beam_momentum.Px(), beam_momentum.Py(), beam_momentum.Pz(), beam_position.X(), beam_position.Y(), beam_position.Z(), -1, true, -9e9, 0., 1., kPPrimary);
				trackStack->setPileupParticleFlagForLastTrack(true);
			}
		}
	};

    //generate beam muon
    if(!m_beamGenerator->generateBeam(beam_pdg, beam_energy, beam_position, beam_momentum))
		return false;

    primGen->AddTrack(beam_pdg, beam_momentum.Px(), beam_momentum.Py(), beam_momentum.Pz(), beam_position.X(), beam_position.Y(), beam_position.Z(), -1, true, -9e9, 0., 1., kPPrimary);

	auto trackStack = dynamic_cast<MUonEStack*>(TVirtualMC::GetMC()->GetStack());
	trackStack->setSignalParticleFlagForLastTrack(true);

	//minbias generation and biasing ends here
	if(m_vertexGenerator == nullptr) {

		//add pileup and finish
		if(m_generationConfiguration.pileupMean() > 0)
			addPileup();

		++m_eventsCounter;
		return true;
	}


    TVector3 vertex_position;
	std::vector<Int_t> outgoing_pdg;
    std::vector<TVector3> outgoing_momenta;

	//beam missed the target
	if(0 != findInteractionPoint(beam_position, beam_momentum, vertex_position)) {

		//add pileup and finish
		if(m_generationConfiguration.pileupMean() > 0)
			addPileup();

		// reset the MESMER event
		if (0 == m_generationConfiguration.vertexGeneratorConfiguration()->baseType().compare("Mesmer"))
		  m_vertexGenerator->Clear();

		++m_eventsCounter;
		return true;
	}

    trackStack->addStationIndexForLastTrack(m_vertexStationIndex);
    trackStack->addStopAtTargetDecisionForLastTrack(true);

	//beam hit the target; add pileup and continue
	if(m_generationConfiguration.pileupMean() > 0)
		addPileup();	   	

	if(0 == m_vertexGenerator->generateOutgoingParticlesKinematics(beam_energy, beam_momentum, outgoing_pdg, outgoing_momenta)) {

		//outgoing particles generated succesfully
		++m_verticesCounter;

		for(Int_t particle_index = 0; particle_index < outgoing_pdg.size(); ++particle_index) {

			auto pdg = outgoing_pdg[particle_index];
			auto momentum = outgoing_momenta[particle_index];

			primGen->AddTrack(pdg, momentum.Px(), momentum.Py(), momentum.Pz(), vertex_position.X(), vertex_position.Y(), vertex_position.Z(), 1, true, -9e9, 0., 1., TMCProcess::kPCoulombScattering);
			trackStack->addStationIndexForLastTrack(m_vertexStationIndex);	
			trackStack->setSignalParticleFlagForLastTrack(true);		
		}

	} else {

        if(failed_attempts++ > 100) {

            LOG(error) << "Vertex generation stuck on a single event. Please check your settings and try again.";
            return false;
        }	

		goto EVENT_GENERATION_START;
	}

	++m_eventsCounter;
    return true;
}


Int_t MUonEGenerator::findInteractionPoint(TVector3 const& beamPosition, TVector3 const& beamMomentum, TVector3& vertexPosition) {

	//vector in the direction of the beam momentum
	TVector3 beam_norm(beamMomentum);
	//normalize so that Z coordinate is equal to 1, that is the vector can be multiplied by Z distance directly to get the position in X/Y
	beam_norm *= 1/beam_norm.Z();

	//if beam misses the target completely, return 1
	TVector3 beam_position_at_target_start = beamPosition + beam_norm * (m_targetMinZ - beamPosition.Z());

	if(beam_position_at_target_start.X() > m_targetMaxX || beam_position_at_target_start.X() < m_targetMinX)
		return 1;
	if(beam_position_at_target_start.Y() > m_targetMaxY || beam_position_at_target_start.Y() < m_targetMinY)
		return 1;		

	TVector3 beam_position_at_target_end = beamPosition + beam_norm * (m_targetMaxZ - beamPosition.Z());

	//beam went straight through the target
	if(beam_position_at_target_end.X() < m_targetMaxX && beam_position_at_target_end.X() > m_targetMinX &&
		beam_position_at_target_end.Y() < m_targetMaxY && beam_position_at_target_end.Y() > m_targetMinY){

			//randomly choose interaction position for Z within the target
			vertexPosition = beamPosition + beam_norm * (gRandom->Uniform(m_targetMinZ, m_targetMaxZ) - beamPosition.Z());
		}
	//beam went in the target but left through the side, not through the back
	else {

		//should be very rare, so this cheated solution in place of finding an intersection should be enough 
		Double_t max_z = m_targetMaxZ;

		//try to search for interaction Z in the full target range; if selected interaction point is outside of the target
		//set new maximum Z to the Z of the chosen interaction point
		//should converge on the correct range quickly; counter just for safety
		Int_t failed_attempts = 0;
		do {
			
			if(failed_attempts++ > 50) return 2;

			vertexPosition = beamPosition + beam_norm * (gRandom->Uniform(m_targetMinZ, max_z) - beamPosition.Z());
			max_z = vertexPosition.Z();

		} while(
			vertexPosition.X() > m_targetMaxX || vertexPosition.X() < m_targetMinX
			|| vertexPosition.Y() > m_targetMaxY || vertexPosition.Y() < m_targetMinY
		);
	}

    return 0;    
}

ClassImp(MUonEGenerator)
