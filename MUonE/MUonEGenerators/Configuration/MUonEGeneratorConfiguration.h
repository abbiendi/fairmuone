#ifndef MUONEGENERATORCONFIGURATION_H
#define MUONEGENERATORCONFIGURATION_H

#include "Rtypes.h"
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

class MUonEGeneratorConfiguration {

public:

    MUonEGeneratorConfiguration(std::string baseType = "")
        : m_baseType(baseType)
        {}

    virtual Bool_t readConfiguration(YAML::Node const& config, std::string opt) = 0;
    virtual void logCurrentConfiguration() const = 0;
    virtual void resetDefaultConfiguration() = 0;

    std::string baseType() const {return m_baseType;}

    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    //kept here to be accessible from run macro
    std::vector<std::string> const& particlesToBias() const {return m_particlesToBias;}
    std::vector<std::vector<std::string>> const& processesToBias() const {return m_processesToBias;}



protected:

    Bool_t m_configuredCorrectly{false};

    std::string m_baseType;

    //kept here to be accessible from run macro
    std::vector<std::string> m_particlesToBias;
    std::vector<std::vector<std::string>> m_processesToBias;    

    ClassDef(MUonEGeneratorConfiguration, 2)
};

#endif //MUONEGENERATORCONFIGURATION_H


