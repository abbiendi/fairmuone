#ifndef MUONEBEAMGENERATORCONFIGURATION_H
#define MUONEBEAMGENERATORCONFIGURATION_H

#include "MUonEGeneratorConfiguration.h"

class MUonEBeamGeneratorConfiguration : public MUonEGeneratorConfiguration {

public:

    MUonEBeamGeneratorConfiguration(std::string baseType = "")
        : MUonEGeneratorConfiguration(baseType)
        {}

    virtual Bool_t isEnergyValid() const = 0;

};

#endif //MUONEBEAMGENERATORCONFIGURATION_H

