#ifndef MUONEPARTICLEGUNBEAMPROFILECONFIGURATION_H
#define MUONEPARTICLEGUNBEAMPROFILECONFIGURATION_H

#include "MUonEBeamGeneratorConfiguration.h"

#include <string>
#include <yaml-cpp/yaml.h>
#include "Rtypes.h"

class MUonEParticleGunBeamProfileConfiguration : public MUonEBeamGeneratorConfiguration {

public:

    MUonEParticleGunBeamProfileConfiguration();

    Int_t pdg() const {return m_pdg;}

    Bool_t massSet() const {return m_massSet;}
    Double_t mass() const {return m_mass;}

    std::string beamProfile() const {return m_beamProfile;}
    Double_t xOffset() const {return m_xOffset;}
    Double_t yOffset() const {return m_yOffset;}

    virtual Bool_t isEnergyValid() const override {
        return m_massSet;
    }

    virtual Bool_t readConfiguration(YAML::Node const& config, std::string opt) override;
    virtual void logCurrentConfiguration() const override;
    virtual void resetDefaultConfiguration() override;


protected:

    Int_t m_pdg{0};

    Bool_t m_massSet{false};
    Double_t m_mass{0};

    std::string m_beamProfile;
    Double_t m_xOffset{0};
    Double_t m_yOffset{0};


    ClassDef(MUonEParticleGunBeamProfileConfiguration, 2)

};

#endif //MUONEPARTICLEGUNBEAMPROFILECONFIGURATION_H

