#include "MUonEVertexGeneratorForceCollisionBiasing.h"

#include "MUonEForceCollisionBiasingOperator.h"

#include <iostream>

#include <fairlogger/Logger.h>

#include <G4AnalysisUtilities.hh>
#include <G4LogicalVolume.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4Material.hh>

MUonEVertexGeneratorForceCollisionBiasing::MUonEVertexGeneratorForceCollisionBiasing()
    : MUonEVertexGenerator("ForceCollisionBiasing")
    {
		resetDefaultConfiguration();
	}

MUonEVertexGeneratorForceCollisionBiasing::~MUonEVertexGeneratorForceCollisionBiasing() {
}

Int_t MUonEVertexGeneratorForceCollisionBiasing::generateOutgoingParticlesKinematics(Double_t beamEnergy, TVector3 const& beamMomentum, std::vector<Int_t>& outgoingPDG, std::vector<TVector3>& outgoingMomenta) {

	return 0;
}

void MUonEVertexGeneratorForceCollisionBiasing::attachOperatorToVolume(std::string name) {

	G4LogicalVolumeStore* lvStore = G4LogicalVolumeStore::GetInstance();

	for (G4int i = 0; i < G4int(lvStore->size()); i++) {
			G4LogicalVolume* lv = (*lvStore)[i];
			G4String volumeName = lv->GetName();

			if(0 == volumeName.compareTo(name)) {

				MUonEForceCollisionBiasingOperator* op = new MUonEForceCollisionBiasingOperator(m_configuration.particlesToBias());
				op->AttachTo(lv);
			}
	}

}



Bool_t MUonEVertexGeneratorForceCollisionBiasing::setConfiguration(const MUonEGeneratorConfiguration* config) {

	resetDefaultConfiguration();
	m_configuredCorrectly = true;
	m_configuredCorrectly = config->configuredCorrectly();

	m_configuration = *dynamic_cast<const MUonEVertexGeneratorForceCollisionBiasingConfiguration*>(config);

    if(!m_configuredCorrectly)
        LOG(info) << "ForceCollisionBiasing configuration contains errors. Please fix them and try again.";

	return m_configuredCorrectly;
}

void MUonEVertexGeneratorForceCollisionBiasing::logCurrentConfiguration() const {

	LOG(info) << "name: " << m_name;
	m_configuration.logCurrentConfiguration();
}

void MUonEVertexGeneratorForceCollisionBiasing::resetDefaultConfiguration() {

	m_configuredCorrectly = false;

	m_configuration.resetDefaultConfiguration();

}


ClassImp(MUonEVertexGeneratorForceCollisionBiasing)

