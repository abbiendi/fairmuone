#ifndef MUONEFORCECOLLISIONBIASINGOPEATOR_H
#define MUONEFORCECOLLISIONBIASINGOPEATOR_H

#include "Rtypes.h"
#include "G4Track.hh"

#include <string>

#include <G4VBiasingOperator.hh>
#include <G4BOptrForceCollision.hh>
#include <G4ParticleDefinition.hh>

#include <unordered_map>

//based on example GB02 in G4

class MUonEForceCollisionBiasingOperator : public G4VBiasingOperator {

public:

    MUonEForceCollisionBiasingOperator(std::vector<std::string> const& particlesToBias);

    virtual G4VBiasingOperation* ProposeNonPhysicsBiasingOperation(const G4Track* track, const G4BiasingProcessInterface* callingProcess) final;
    virtual G4VBiasingOperation* ProposeOccurenceBiasingOperation (const G4Track* track, const G4BiasingProcessInterface* callingProcess) final;
    virtual G4VBiasingOperation* ProposeFinalStateBiasingOperation(const G4Track* track, const G4BiasingProcessInterface* callingProcess) final;

    virtual void StartTracking(const G4Track* track) final;  


private:


    void OperationApplied( const G4BiasingProcessInterface* callingProcess, G4BiasingAppliedCase biasingCase, G4VBiasingOperation* operationApplied, const G4VParticleChange* particleChangeProduced) final;
    void OperationApplied( const G4BiasingProcessInterface* callingProcess, G4BiasingAppliedCase biasingCase, G4VBiasingOperation* occurenceOperationApplied, G4double weightForOccurenceInteraction, G4VBiasingOperation* finalStateOperationApplied, const G4VParticleChange* particleChangeProduced) final;
    
    void ExitBiasing(const G4Track*, const G4BiasingProcessInterface*) final;


    std::unordered_map<const G4ParticleDefinition*, G4BOptrForceCollision*> m_particleOperatorMap;
    G4BOptrForceCollision* m_currentOperator = nullptr;


    ClassDef(MUonEForceCollisionBiasingOperator, 2)

};


#endif //MUONEFORCECOLLISIONBIASINGOPEATOR_H



