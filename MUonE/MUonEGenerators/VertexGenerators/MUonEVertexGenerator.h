#ifndef MUONEVERTEXGENERATOR_H
#define MUONEVERTEXGENERATOR_H

#include "Rtypes.h"
#include "TVector3.h"

#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

#include "MUonEGeneratorConfiguration.h"

class MUonEVertexGenerator {

public:

    MUonEVertexGenerator(std::string name = "MUonEVertexGenerator") 
        : m_name(name)
        {}


    virtual Int_t generateOutgoingParticlesKinematics(Double_t beamEnergy, TVector3 const& beamMomentum, std::vector<Int_t>& outgoingPDG, std::vector<TVector3>& outgoingMomenta) = 0;

    virtual Bool_t setConfiguration(const MUonEGeneratorConfiguration* config) = 0;
    virtual void logCurrentConfiguration() const = 0;

    virtual void resetDefaultConfiguration() = 0;

    virtual Bool_t Init() = 0; 
    virtual Bool_t Finalize(Int_t numberOfEvents = 0, Int_t numberOfVertices = 0) = 0; 
    virtual void Clear() {};


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}
    std::string name() const {return m_name;}
    
protected:

    Bool_t m_configuredCorrectly{false};
    std::string m_name;

    const Double_t MUON_MASS = 105.6583715  * 1.e-3;
    const Double_t MUON_MASS_SQUARED = MUON_MASS * MUON_MASS;

    const Double_t ELECTRON_MASS = 0.5109989461 * 1.e-3;
    const Double_t ELECTRON_MASS_SQUARED = ELECTRON_MASS * ELECTRON_MASS;

	ClassDef(MUonEVertexGenerator, 2)
};

#endif //MUONEVERTEXGENERATOR_H

