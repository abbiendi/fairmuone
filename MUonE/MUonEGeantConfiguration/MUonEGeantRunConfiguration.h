#ifndef MUONEGEANTRUNCONFIGURATION_H
#define MUONEGEANTRUNCONFIGURATION_H

#include "TG4RunConfiguration.h"

#include <vector>
#include <string>

class MUonEGeantRunConfiguration : public TG4RunConfiguration {

public:

MUonEGeantRunConfiguration(const TString& userGeometry,
    const TString& physicsList = "emStandard",
    const TString& specialProcess = "stepLimiter",
    Bool_t specialStacking = false, Bool_t mtApplication = true,
    std::vector<std::string> const& particlesToBias = {},
    std::vector<std::vector<std::string>> const& processesToBias = {});


~MUonEGeantRunConfiguration() = default;


virtual G4VUserPhysicsList* CreatePhysicsList() override;


private:

  std::vector<std::string> m_particlesToBias;
  std::vector<std::vector<std::string>> m_processesToBias;


  ClassDef(MUonEGeantRunConfiguration, 2)

};

#endif //MUONEGEANTRUNCONFIGURATION_H 


