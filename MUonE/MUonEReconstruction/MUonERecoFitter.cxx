#include "MUonERecoFitter.h"

#include "MUonERecoModule.h"

#include <cmath>

#include "TH1F.h"
#include "TMath.h"
#include "TMatrixD.h"
#include "TVectorD.h"

#include "Math/SMatrix.h"

#include "TGraphErrors.h"
#include "TFitResult.h"

#include <fairlogger/Logger.h>
#include <algorithm>

#include <boost/container/static_vector.hpp>

#include "ElasticState.h"

MUonERecoFitter::MUonERecoFitter() {}


Int_t MUonERecoFitter::addFitInfo(MUonERecoTrack2D& track, Double_t p, std::vector<MUonERecoModule> const& modules, MUonEReconstructionConfiguration const& config) const {

	//no hits added, slope and x0 are the same as calculated on initialization
	if(2 == track.numberOfHits()) {
	
		track.setChi2(0);
		return 0;

	} else if (track.numberOfHits() < 2)
		LOG(error) << "Error: trying to fit 2D track with less than 2 hits!";


	auto hits = track.hits();

	if(config.usePerpendicularDistanceFor2DTrackFit()) {

		//https://arxiv.org/pdf/hep-ex/9406006

		TVectorD X(modules.size());
		X.Zero();

		TMatrixD H(modules.size(), 2);
		H.Zero();

		//A order x0 ax		

		//X - HA -> posperp - [ax*z + bx]
		for(auto const& h : hits) {

			auto const& m = h.module();
			auto indx = m.index();

			X(indx) = h.positionPerpendicular();
			H(indx, 0) =  1;
			H(indx, 1) =  h.z();	
		}
		
		TMatrixD errors = calculateErrorMatrix(modules, getHitZPositions<MUonERecoTrack2D>(track, hits, modules), p, config.disableNonDiagonalCovarianceMatrixTerms());
		TMatrixD V = errors;

		V.Invert();
		if(!V.IsValid())
			return 1;	

		TMatrixD HT = H;
		HT.T();

		TMatrixD HTVI = HT * V;

		TMatrixD cov = HTVI * H;
		
		cov.Invert();
		if(!cov.IsValid())
			return 1;		 

		
		auto HTVIX = HTVI * X;
		auto result = cov * HTVIX;

		auto XHR = X - H * result;
		auto chi2a = V * XHR;
		auto chi2 = XHR * chi2a;

		track.setChi2(chi2);

		track.setX0(result(0), TMath::Sqrt(cov(0,0)));
		track.setSlope(result(1), TMath::Sqrt(cov(1,1)));

		return 0;

	} else {

		//track fit using distance in module plane
		for(Int_t iter = 0; iter < config.numberOfIterationsInTrackFit(); ++iter) {


			//https://arxiv.org/pdf/hep-ex/9406006

			TVectorD X(modules.size());
			X.Zero();

			TMatrixD H(modules.size(), 4);
			H.Zero();


			//A order x0 ax y0 ay
			/*
				https://indico.cern.ch/event/1401448/contributions/5891071/attachments/2830668/4945820/updateAlignment_weekly_RP.pdf

				mod_pos = [xmod, ymod, zmod]
				mod_norm = [modnormx, modnormy, modnormz]
				mod_meas_dir = [moddirx, moddiry, moddirz]
				track_free_coeff = [x0, y0, z0]
				track_dir = [xslope, yslope, 1]



				t = (mod_pos - track_free_coeff) . mod_norm / (track_dir . mod_norm)
				t = ( (xmod - x0) * modnormx + (ymod - y0) * modnormy + (zmod - z0) * modnormz) ) / (xslope * modnormx + yslope * modnormy + modnormz)

				dt/dx0 = -1 * modnormx / (track_dir . mod_norm)
				dt/dxslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormx / (track_dir . mod_norm)^2

				dt/dy0 = -1 * modnormy / (track_dir . mod_norm)
				dt/dyslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormy / (track_dir . mod_norm)^2


				x(p) = (track_free_coeff + track_dir * t - mod_pos) . mod_meas_dir
				x(p) = (x0 + xslope * t - xmod) * moddirx + (y0 + yslope * t - ymod) * moddiry + (z0 + t - zmod) * moddirz

				dx(p)/dx0 = ([1, 0, 0] + track_dir * dt/dx0) . mod_meas_dir
				dx(p)/dxslope = ([1, 0, 0] * t + track_dir * dt/dxslope) . mod_meas_dir

				dx(p)/dy0 = ([0, 1, 0] + track_dir * dt/dy0) . mod_meas_dir
				dx(p)/dyslope = ([0, 1, 0] * t + track_dir * dt/dyslope) . mod_meas_dir


				p = [x0 ax y0 ay] = A

				xlin(p) = x(p0) + gradx(p0) . (p - p0) = [x(p0) - gradx(p0) . p0] + [gradx(p0) . p] 

				chi2 = pos - xlin(p) = {pos - [x(p0) - gradx(p0) . p0]} - [gradx(p0) . p]
			*/		

			//X - HA -> pos - xlin(p) -> X = {pos - [x(p0) - gradx(p0) . p0]}; H = gradx(p0)

			//initial track parameters
			auto const& track_free_coeff = track.freeCoefficientsVector();
			auto const& track_dir = track.directionVector();			

			for(auto const& h : hits) {

				auto const& hit_module = h.module(); 
				auto const& mod_pos = hit_module.positionVector();
				auto const& mod_norm = hit_module.normalVector();
				auto const& mod_meas_dir = hit_module.measurementDirection();

				auto indx = hit_module.index();

				auto t_nominator = (mod_pos - track_free_coeff).Dot(mod_norm);
				auto t_denominator = track_dir.Dot(mod_norm);
				auto t = t_nominator / t_denominator;

				Double_t x_p0 = (track_free_coeff + track_dir * t - mod_pos).Dot(mod_meas_dir);

				Double_t dt_dx0 = -1 * mod_norm[0] / t_denominator;
				Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
				Double_t dt_dy0 = -1 * mod_norm[1] / t_denominator;
				Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

				//gradx(p0)
				Double_t dx_dx0 = (TVector3(1,0,0) + track_dir * dt_dx0).Dot(mod_meas_dir);
				Double_t dx_dxslope = (TVector3(t,0,0) + track_dir * dt_dxslope).Dot(mod_meas_dir);
				Double_t dx_dy0 = (TVector3(0,1,0) + track_dir * dt_dy0).Dot(mod_meas_dir);
				Double_t dx_dyslope = (TVector3(0,t,0) + track_dir * dt_dyslope).Dot(mod_meas_dir);

				Double_t gradx_dot_p0 = dx_dx0 * track_free_coeff[0] + dx_dxslope * track_dir[0] + dx_dy0 * track_free_coeff[1] + dx_dyslope * track_dir[1];



				X(indx) = h.position() - (x_p0 - gradx_dot_p0); //free term
				H(indx, 0) = dx_dx0; //x0
				H(indx, 1) = dx_dxslope; //ax
				H(indx, 2) = dx_dy0; //y0
				H(indx, 3) = dx_dyslope; //ay
			}
			
			TMatrixD errors = calculateErrorMatrix(modules, getHitZPositions<MUonERecoTrack2D>(track, hits, modules), p, config.disableNonDiagonalCovarianceMatrixTerms());
			TMatrixD V = errors;

			V.Invert();
			if(!V.IsValid())
				return 1;	

			TMatrixD HT = H;
			HT.T();

			TMatrixD HTVI = HT * V;

			TMatrixD cov = HTVI * H;
			
			cov.Invert();
			if(!cov.IsValid())
				return 1;		 

			
			auto HTVIX = HTVI * X;
			auto result = cov * HTVIX;

			auto XHR = X - H * result;
			auto chi2a = V * XHR;
			auto chi2 = XHR * chi2a;

			track.setChi2(chi2);

			if('x' == track.projection()) {

				track.setX0(result(0), TMath::Sqrt(cov(0,0)));
				track.setSlope(result(1), TMath::Sqrt(cov(1,1)));

			} else if('y' == track.projection()) {

				track.setX0(result(2), TMath::Sqrt(cov(2,2)));
				track.setSlope(result(3), TMath::Sqrt(cov(3,3)));	

			} else {

				LOG(fatal) << "MUonERecoFitter: Trying to fit 2D track, but projection is neither x nor y.";
			}

			

		}

		return 0;
	}

	return 0;
}

Int_t MUonERecoFitter::addFitInfo(MUonERecoTrack3D& track, Double_t p, Bool_t allowTrivial, Double_t maxOutlierChi2, Bool_t enableOutlierRemoval, std::vector<MUonERecoModule> const& modules, MUonEReconstructionConfiguration const& config) const {

	auto hits = track.hitsCopy();

	Bool_t fit_converged = false;
	Int_t number_of_xHits = track.xTrack().numberOfHits();
	Int_t number_of_yHits = track.yTrack().numberOfHits();
	Int_t number_of_stereo_hits = track.numberOfStereoHits();

	Double_t z0 = 0; //track z0, fixed for now, important for where the covariance matrix is calculated

	
	while(!fit_converged) {

		//track becomes trivial/non-defined
		if(0 == number_of_stereo_hits) {

			if(allowTrivial) {

				if(number_of_xHits < 2 || number_of_yHits < 2)
					return 1;

			} else {

				if(number_of_xHits < 3 || number_of_yHits < 3)
					return 1;
			}

		} else {

			if(number_of_xHits < 2 || number_of_yHits < 2)
				return 1;			
		}

		


		if(config.usePerpendicularDistanceFor3DTrackFit()) {

			//https://arxiv.org/pdf/hep-ex/9406006

			TVectorD X(modules.size());
			X.Zero();

			TMatrixD H(modules.size(), 4);
			H.Zero();


			//A order x0 ax y0 ay		

			//X - HA -> posperp - [(ax*(z-z0) + bx)*cosa + (ay*(z-z0) + by)*sina]
			for(auto const& h : hits) {

				auto const& m = h.module();
				auto indx = m.index();

				X(indx) = h.positionPerpendicular();
				H(indx, 0) =  m.angleCosine();
				H(indx, 1) =  (h.z() - z0) * m.angleCosine();
				H(indx, 2) =  m.angleSine();
				H(indx, 3) =  (h.z() - z0) * m.angleSine();			
			}
		
			TMatrixD errors = calculateErrorMatrix(modules, getHitZPositions<MUonERecoTrack3D>(track, hits, modules), p, config.disableNonDiagonalCovarianceMatrixTerms());
			TMatrixD V = errors;

			V.Invert();
			if(!V.IsValid())
				return 1;	

			TMatrixD HT = H;
			HT.T();

			TMatrixD HTVI = HT * V;

			TMatrixD cov = HTVI * H;
		
			cov.Invert();
			if(!cov.IsValid())
				return 1;		 

		
			auto HTVIX = HTVI * X;
			auto result = cov * HTVIX;

			auto XHR = X - H * result;
			auto chi2a = V * XHR;
			auto chi2 = XHR * chi2a;

			track.setChi2(chi2);

										//slope, slope error, x0/y0, x0/y0 error
			track.setXTrackParameters(result(1), TMath::Sqrt(cov(1,1)), result(0), TMath::Sqrt(cov(0,0)));
			track.setYTrackParameters(result(3), TMath::Sqrt(cov(3,3)), result(2), TMath::Sqrt(cov(2,2)));
			track.setLinearFitCovarianceMatrix(cov);

		} else {


			//track fit using distance in module plane
			for(Int_t iter = 0; iter < config.numberOfIterationsInTrackFit(); ++iter) {


				//https://arxiv.org/pdf/hep-ex/9406006

				TVectorD X(modules.size());
				X.Zero();

				TMatrixD H(modules.size(), 4);
				H.Zero();


				//A order x0 ax y0 ay
				/*
					https://indico.cern.ch/event/1401448/contributions/5891071/attachments/2830668/4945820/updateAlignment_weekly_RP.pdf

					mod_pos = [xmod, ymod, zmod]
					mod_norm = [modnormx, modnormy, modnormz]
					mod_meas_dir = [moddirx, moddiry, moddirz]
					track_free_coeff = [x0, y0, z0]
					track_dir = [xslope, yslope, 1]



					t = (mod_pos - track_free_coeff) . mod_norm / (track_dir . mod_norm)
					t = ( (xmod - x0) * modnormx + (ymod - y0) * modnormy + (zmod - z0) * modnormz) ) / (xslope * modnormx + yslope * modnormy + modnormz)

					dt/dx0 = -1 * modnormx / (track_dir . mod_norm)
					dt/dxslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormx / (track_dir . mod_norm)^2

					dt/dy0 = -1 * modnormy / (track_dir . mod_norm)
					dt/dyslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormy / (track_dir . mod_norm)^2


					x(p) = (track_free_coeff + track_dir * t - mod_pos) . mod_meas_dir
					x(p) = (x0 + xslope * t - xmod) * moddirx + (y0 + yslope * t - ymod) * moddiry + (z0 + t - zmod) * moddirz

					dx(p)/dx0 = ([1, 0, 0] + track_dir * dt/dx0) . mod_meas_dir
					dx(p)/dxslope = ([1, 0, 0] * t + track_dir * dt/dxslope) . mod_meas_dir

					dx(p)/dy0 = ([0, 1, 0] + track_dir * dt/dy0) . mod_meas_dir
					dx(p)/dyslope = ([0, 1, 0] * t + track_dir * dt/dyslope) . mod_meas_dir


					p = [x0 ax y0 ay] = A

					xlin(p) = x(p0) + gradx(p0) . (p - p0) = [x(p0) - gradx(p0) . p0] + [gradx(p0) . p] 

					chi2 = pos - xlin(p) = {pos - [x(p0) - gradx(p0) . p0]} - [gradx(p0) . p]
				*/		

				//X - HA -> pos - xlin(p) -> X = {pos - [x(p0) - gradx(p0) . p0]}; H = gradx(p0)

				//initial track parameters
				auto const& track_free_coeff = track.freeCoefficientsVector();
				auto const& track_dir = track.directionVector();			

				for(auto const& h : hits) {

					auto const& hit_module = h.module(); 
					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto indx = hit_module.index();

					auto t_nominator = (mod_pos - track_free_coeff).Dot(mod_norm);
					auto t_denominator = track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (track_free_coeff + track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dx0 = -1 * mod_norm[0] / t_denominator;
					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dy0 = -1 * mod_norm[1] / t_denominator;
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					//gradx(p0)
					Double_t dx_dx0 = (TVector3(1,0,0) + track_dir * dt_dx0).Dot(mod_meas_dir);
					Double_t dx_dxslope = (TVector3(t,0,0) + track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dy0 = (TVector3(0,1,0) + track_dir * dt_dy0).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dx0 * track_free_coeff[0] + dx_dxslope * track_dir[0] + dx_dy0 * track_free_coeff[1] + dx_dyslope * track_dir[1];



					X(indx) = h.position() - (x_p0 - gradx_dot_p0); //free term
					H(indx, 0) = dx_dx0; //x0
					H(indx, 1) = dx_dxslope; //ax
					H(indx, 2) = dx_dy0; //y0
					H(indx, 3) = dx_dyslope; //ay
				}

				TMatrixD errors = calculateErrorMatrix(modules, getHitZPositions<MUonERecoTrack3D>(track, hits, modules), p, config.disableNonDiagonalCovarianceMatrixTerms());
				TMatrixD V = errors;

				V.Invert();
				if(!V.IsValid())
					return 1;	

				TMatrixD HT = H;
				HT.T();

				TMatrixD HTVI = HT * V;

				TMatrixD cov = HTVI * H;
			
				cov.Invert();
				if(!cov.IsValid())
					return 1;		 

			
				auto HTVIX = HTVI * X;
				auto result = cov * HTVIX;

				auto XHR = X - H * result;
				auto chi2a = V * XHR;
				auto chi2 = XHR * chi2a;


				track.setChi2(chi2);

											//slope, slope error, x0/y0, x0/y0 error
				track.setXTrackParameters(result(1), TMath::Sqrt(cov(1,1)), result(0), TMath::Sqrt(cov(0,0)));
				track.setYTrackParameters(result(3), TMath::Sqrt(cov(3,3)), result(2), TMath::Sqrt(cov(2,2)));
				track.setLinearFitCovarianceMatrix(cov);
			}
		}



		if(enableOutlierRemoval && maxOutlierChi2 > 0) {

			//find maximum outlier
			Double_t max_chi2 = 0;
			auto outlier_iter = hits.end();

			TMatrixD errors_out = calculateErrorMatrix(modules, getHitZPositions<MUonERecoTrack3D>(track, hits, modules), p, config.disableNonDiagonalCovarianceMatrixTerms());
			
			for(auto it = hits.begin(); it != hits.end(); ++it) {

				Double_t chi2 = config.usePerpendicularDistanceFor3DTrackFit() ? track.perpendicularDistanceToHit(*it) * track.perpendicularDistanceToHit(*it) : track.distanceToHitInModulePlane(*it) * track.distanceToHitInModulePlane(*it); 
				chi2 /= errors_out((*it).moduleIndex(), (*it).moduleIndex());

				if(chi2 > max_chi2) {
					
					outlier_iter = it;
					max_chi2 = chi2;
				}
			}


			if(max_chi2 < maxOutlierChi2) {

				fit_converged = true;

			} else {

				hits.erase(outlier_iter); //remove outlier from hit collection

				if('x' == outlier_iter->module().projection())
					--number_of_xHits;
				else if('y' == outlier_iter->module().projection())
					--number_of_yHits;
				else if(outlier_iter->module().isStereo())
					--number_of_stereo_hits;
			}			
		} else {

			fit_converged = true;
		}


	} //!while(!fit_converged)

	//if any of the hits were removed from the copy
	if(hits.size() < track.numberOfHits())
		track.setHits(hits);

	return 0;
}


Int_t MUonERecoFitter::addFitInfo(MUonERecoTarget const& target, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, MUonERecoVertex& vertex, MUonEReconstructionConfiguration const& config) const {

	//PID based on initial theta angles

	auto& incoming_ref = vertex.incomingTrackRef();
	auto& electron_ref = vertex.assumedElectronTrackRef();
	auto& muon_ref = vertex.assumedMuonTrackRef();

	auto theta_e = vertex.assumedElectronTrackTheta();

	const Double_t mass_e  = 0.51099895*1e-3; // GeV
	const Double_t mass_mu = 105.6583755*1e-3; // GeV


	//set assumed particle momenta; if -1 no MS correction will be applied
	Double_t incoming_momentum = -1;
	Double_t muon_momentum = -1;
	Double_t electron_momentum = -1;

	if(config.addBaselineMSCorrectionToAllTracks()) {

		incoming_momentum = config.MSCorrectionAssumedBeamEnergy();
		muon_momentum = config.MSCorrectionAssumedBeamEnergy();
		electron_momentum = config.MSCorrectionAssumedBeamEnergy();
	}
  
	//if full correction is enabled, override the momenta
	MuE::ElasticState mue(config.MSCorrectionAssumedBeamEnergy(), mass_mu, mass_e, theta_e*1e3);

	if(config.refitIncomingTrackWithMSCorrection()) {

		incoming_momentum = config.MSCorrectionAssumedBeamEnergy();
	}
	
	if(config.refitMuonTrackWithMSCorrection()) {

		muon_momentum = mue.GetEnergy_mu();
	}

	if(config.refitElectronTrackWithMSCorrection()) {

		electron_momentum = mue.GetEnergy_e();
	}


	if(!vertex.hitsReassigned()) {

		//first time processing the vertex, if refitting disabled the tracks were already fitted previously
		if(config.refitIncomingTrackWithMSCorrection()) {

			addFitInfo(incoming_ref, incoming_momentum, true, -1, false, modulesPerSector[incoming_ref.sector()], config);
			addFirstStateInfo(incoming_ref, true, incoming_momentum);	
		}

		if(config.refitMuonTrackWithMSCorrection()) {

			addFitInfo(muon_ref, muon_momentum, true, -1, false, modulesPerSector[muon_ref.sector()], config);
			addFirstStateInfo(muon_ref, true, muon_momentum);	
		}

		if(config.refitElectronTrackWithMSCorrection()) {

			addFitInfo(electron_ref, electron_momentum, true, -1, false, modulesPerSector[electron_ref.sector()], config);
			addFirstStateInfo(electron_ref, true, electron_momentum);			
		}
	
	} else {

		//hits were reassigned after first vertex fit
		//force refit of tracks with new hits for the position fit
			addFitInfo(incoming_ref, incoming_momentum, true, -1, false, modulesPerSector[incoming_ref.sector()], config);
			addFirstStateInfo(incoming_ref, true, incoming_momentum);				

			addFitInfo(muon_ref, muon_momentum, true, -1, false, modulesPerSector[muon_ref.sector()], config);
			addFirstStateInfo(muon_ref, true, muon_momentum);				


			addFitInfo(electron_ref, electron_momentum, true, -1, false, modulesPerSector[electron_ref.sector()], config);
			addFirstStateInfo(electron_ref, true, electron_momentum);				
	}
	


	TVector3 position_positionfit; 
	TVector3 positionError_positionfit; 
	ROOT::Math::SMatrix<Double_t, 3> covarianceMatrix_positionfit;
	Bool_t position_fit_status = fitVertexPosition({incoming_ref, muon_ref, electron_ref}, position_positionfit, positionError_positionfit, covarianceMatrix_positionfit);

	vertex.setPositionFitStatus(position_fit_status);

	if(position_fit_status) {

		vertex.setPositionPositionFit(position_positionfit[0], positionError_positionfit[0], position_positionfit[1], positionError_positionfit[1], position_positionfit[2], positionError_positionfit[2]);
		vertex.setPositionFitCovarianceMatrix(covarianceMatrix_positionfit);
	
	} else {

		LOG(info) << "Position fit failed for a vertex. If it was to be used for the KF, middle of the target will be used instead";
	}


	if(!config.runKinematicFit()) {

		vertex.setChi2(incoming_ref.chi2() + electron_ref.chi2() + muon_ref.chi2());
		vertex.recalculateAngles();
		return 0;
	}


	//start KF
	auto vertex_z_position = target.z();
	Double_t vertex_x_position = -999;
	Double_t vertex_y_position = -999;

	//thickness assumed for MS correction, default to half if position fitted during KF or independent position fit failed
	Double_t target_thickness_MS_incoming = 0.5 * (target.zMax() - target.zMin());
	Double_t target_thickness_MS_outgoing = target_thickness_MS_incoming;


	if(config.useFittedZPositionInKinematicFit() || config.useFittedPositionInKinematicFit()) {

		if(position_fit_status) {

			vertex_x_position = position_positionfit[0];
			vertex_y_position = position_positionfit[1];
			vertex_z_position = position_positionfit[2];
			
			if(config.restrictVertexPositionToTarget()) {

				if(vertex_z_position < target.zMin())
					vertex_z_position = target.zMin();
				else if(vertex_z_position > target.zMax())
					vertex_z_position = target.zMax();

				if(vertex_x_position < target.xMin())
					vertex_x_position = target.xMin();
				else if(vertex_x_position > target.xMax())
					vertex_x_position = target.xMax();		

				if(vertex_y_position < target.yMin())
					vertex_y_position = target.yMin();
				else if(vertex_y_position > target.yMax())
					vertex_y_position = target.yMax();				
			}

			target_thickness_MS_incoming = vertex_z_position <= target.zMin() ? -1 : vertex_z_position - target.zMin();
			target_thickness_MS_outgoing = vertex_z_position >= target.zMax() ? -1 : target.zMax() - vertex_z_position;
		}
	}

	//not used if thickness is -1
	Double_t target_position_MS_incoming = target.zMin() + 0.5 * target_thickness_MS_incoming;
	Double_t target_position_MS_outgoing = target.zMax() - 0.5 * target_thickness_MS_outgoing;

	if(!config.addMSCorrectionFromTargetInKinematicFit()) {

		//disable
		target_thickness_MS_incoming = -1;
		target_thickness_MS_outgoing = -1;
	}


	auto const& incoming_hits = incoming_ref.hitsCopy();
	auto const& muon_hits = muon_ref.hitsCopy();
	auto const& electron_hits = electron_ref.hitsCopy();

	auto const& incoming_modules = modulesPerSector[incoming_ref.sector()];
	auto const& outgoing_modules = modulesPerSector[muon_ref.sector()];


	auto incoming_errors = calculateErrorMatrix(incoming_modules, getHitZPositions<MUonERecoTrack3D>(incoming_ref, incoming_hits, incoming_modules), incoming_momentum, config.disableNonDiagonalCovarianceMatrixTerms(), target_thickness_MS_incoming, target_position_MS_incoming, target, true);
	auto muon_errors = calculateErrorMatrix(outgoing_modules, getHitZPositions<MUonERecoTrack3D>(muon_ref, muon_hits, outgoing_modules), muon_momentum, config.disableNonDiagonalCovarianceMatrixTerms(), target_thickness_MS_outgoing, target_position_MS_outgoing, target, false);
	auto electron_errors = calculateErrorMatrix(outgoing_modules, getHitZPositions<MUonERecoTrack3D>(electron_ref, electron_hits, outgoing_modules), electron_momentum, config.disableNonDiagonalCovarianceMatrixTerms(), target_thickness_MS_outgoing, target_position_MS_outgoing, target, false);

	TMatrixD combined_errors(incoming_errors.GetNrows() + muon_errors.GetNrows() + electron_errors.GetNrows(), incoming_errors.GetNrows() + muon_errors.GetNrows() + electron_errors.GetNrows());
	combined_errors.Zero();

	TMatrixDSub(combined_errors, 0, incoming_errors.GetNrows() - 1, 0, incoming_errors.GetNrows() - 1) = incoming_errors;
	TMatrixDSub(combined_errors, incoming_errors.GetNrows(), incoming_errors.GetNrows() + muon_errors.GetNrows() - 1, incoming_errors.GetNrows(), incoming_errors.GetNrows() + muon_errors.GetNrows() - 1) = muon_errors;
	TMatrixDSub(combined_errors, incoming_errors.GetNrows() + muon_errors.GetNrows(), combined_errors.GetNrows() - 1, incoming_errors.GetNrows() + muon_errors.GetNrows(), combined_errors.GetNrows() - 1) = electron_errors;
	


	if(config.useFittedPositionInKinematicFit() && position_fit_status) {


		if(config.usePerpendicularDistanceFor3DTrackFit()) {

			//https://arxiv.org/pdf/hep-ex/9406006

			TVectorD X(combined_errors.GetNrows());
			X.Zero();

			TMatrixD H(combined_errors.GetNrows(), 6);
			H.Zero();

			//A matrix order - ax_inc ay_inc ax_muon ay_muon ax_electron ay_electron
			
			//X - HA -> posperp - [(ax*(z-zvertex) + xvertex)*cosa + (ay*(z-zvertex) + yvertex)*sina]
			for(auto const& h : incoming_hits) {

				auto const& m = h.module();
				auto indx = m.index();

				X(indx) = h.positionPerpendicular() - vertex_x_position * m.angleCosine() - vertex_y_position * m.angleSine();

				H(indx, 0) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 1) =  (h.z() - vertex_z_position) * m.angleSine();	
			}


			for(auto const& h : muon_hits) {

				auto const& m = h.module();
				auto indx = incoming_errors.GetNrows() + m.index();

				X(indx) = h.positionPerpendicular() - vertex_x_position * m.angleCosine() - vertex_y_position * m.angleSine();

				H(indx, 2) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 3) =  (h.z() - vertex_z_position) * m.angleSine();	
			}


			for(auto const& h : electron_hits) {

				auto const& m = h.module();
				auto indx = incoming_errors.GetNrows() + muon_errors.GetNrows() + m.index();

				X(indx) = h.positionPerpendicular() - vertex_x_position * m.angleCosine() - vertex_y_position * m.angleSine();

				H(indx, 4) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 5) =  (h.z() - vertex_z_position) * m.angleSine();	
			}


			
			auto V = combined_errors;
			V.Invert();
			if(!V.IsValid())
				return 1;	

			TMatrixD HT = H;
			HT.T();

			TMatrixD HTVI = HT * V;

			TMatrixD cov = HTVI * H;
		
			cov.Invert();
			if(!cov.IsValid())
				return 1;		 

		
			auto HTVIX = HTVI * X;
			auto result = cov * HTVIX;


			auto XHR = X - H * result;
			auto chi2a = V * XHR;
			auto chi2 = XHR * chi2a;

			vertex.setChi2(chi2);


			vertex.setPositionKinematicFit(vertex_x_position, positionError_positionfit[0], vertex_y_position, positionError_positionfit[1], vertex_z_position, positionError_positionfit[2]);

			incoming_ref.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), vertex_x_position, positionError_positionfit[0]);
			incoming_ref.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), vertex_y_position, positionError_positionfit[1]);
			incoming_ref.setZ0(vertex_z_position);

			muon_ref.setXTrackParameters(result(2), TMath::Sqrt(cov(2,2)), vertex_x_position, positionError_positionfit[0]);
			muon_ref.setYTrackParameters(result(3), TMath::Sqrt(cov(3,3)), vertex_y_position, positionError_positionfit[1]);
			muon_ref.setZ0(vertex_z_position);

			electron_ref.setXTrackParameters(result(4), TMath::Sqrt(cov(4,4)), vertex_x_position, positionError_positionfit[0]);
			electron_ref.setYTrackParameters(result(5), TMath::Sqrt(cov(5,5)), vertex_y_position, positionError_positionfit[1]);
			electron_ref.setZ0(vertex_z_position);


			vertex.setLinearFitCovarianceMatrix(cov);

			vertex.recalculateAngles();	

		} else {


			for(Int_t iter = 0; iter < config.numberOfIterationsInVertexFit(); ++iter) {

				TVectorD X(combined_errors.GetNrows());
				X.Zero();

				TMatrixD H(combined_errors.GetNrows(), 6);
				H.Zero();

				//A matrix order - ax_inc ay_inc ax_muon ay_muon ax_electron ay_electron
				
				/*
					https://indico.cern.ch/event/1401448/contributions/5891071/attachments/2830668/4945820/updateAlignment_weekly_RP.pdf

					mod_pos = [xmod, ymod, zmod]
					mod_norm = [modnormx, modnormy, modnormz]
					mod_meas_dir = [moddirx, moddiry, moddirz]
					track_free_coeff = [x0, y0, z0] -- fixed to full vertex position from position fit 
					track_dir = [xslope, yslope, 1]



					t = (mod_pos - track_free_coeff) . mod_norm / (track_dir . mod_norm)
					t = ( (xmod - x0) * modnormx + (ymod - y0) * modnormy + (zmod - z0) * modnormz) ) / (xslope * modnormx + yslope * modnormy + modnormz)

					dt/dxslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormx / (track_dir . mod_norm)^2
					dt/dyslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormy / (track_dir . mod_norm)^2


					x(p) = (track_free_coeff + track_dir * t - mod_pos) . mod_meas_dir
					x(p) = (x0 + xslope * t - xmod) * moddirx + (y0 + yslope * t - ymod) * moddiry + (z0 + t - zmod) * moddirz

					dx(p)/dxslope = ([1, 0, 0] * t + track_dir * dt/dxslope) . mod_meas_dir
					dx(p)/dyslope = ([0, 1, 0] * t + track_dir * dt/dyslope) . mod_meas_dir

					for a single track:

					p = [ax ay] = A

					xlin(p) = x(p0) + gradx(p0) . (p - p0) = [x(p0) - gradx(p0) . p0] + [gradx(p0) . p] 

					chi2 = pos - xlin(p) = {pos - [x(p0) - gradx(p0) . p0]} - [gradx(p0) . p]
				*/		

				//X - HA -> pos - xlin(p) -> X = {pos - [x(p0) - gradx(p0) . p0]}; H = gradx(p0)


				incoming_ref.setX0(vertex_x_position, positionError_positionfit[0]);
				incoming_ref.setY0(vertex_y_position, positionError_positionfit[1]);
				incoming_ref.setZ0(vertex_z_position);
				auto const& incoming_track_free_coeff = incoming_ref.freeCoefficientsVector();
				auto const& incoming_track_dir = incoming_ref.directionVector();	

				
				for(auto const& h : incoming_hits) {


					auto const& hit_module = h.module(); 
					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto indx = hit_module.index();

					auto t_nominator = (mod_pos - incoming_track_free_coeff).Dot(mod_norm);
					auto t_denominator = incoming_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (incoming_track_free_coeff + incoming_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					Double_t dx_dxslope = (TVector3(t,0,0) + incoming_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + incoming_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dxslope * incoming_track_dir[0] + dx_dyslope * incoming_track_dir[1];

					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 0) =  dx_dxslope; //ax
					H(indx, 1) =  dx_dyslope;	//ay
				}

				muon_ref.setX0(vertex_x_position, positionError_positionfit[0]);
				muon_ref.setY0(vertex_y_position, positionError_positionfit[1]);
				muon_ref.setZ0(vertex_z_position);
				auto const& muon_track_free_coeff = muon_ref.freeCoefficientsVector();
				auto const& muon_track_dir = muon_ref.directionVector();	



				for(auto const& h : muon_hits) {

					auto const& hit_module = h.module(); 
					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto indx = incoming_errors.GetNrows() + hit_module.index();

					auto t_nominator = (mod_pos - muon_track_free_coeff).Dot(mod_norm);
					auto t_denominator = muon_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (muon_track_free_coeff + muon_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					Double_t dx_dxslope = (TVector3(t,0,0) + muon_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + muon_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dxslope * muon_track_dir[0] + dx_dyslope * muon_track_dir[1];

					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 2) =  dx_dxslope; //ax
					H(indx, 3) =  dx_dyslope;	//ay
				}

				electron_ref.setX0(vertex_x_position, positionError_positionfit[0]);
				electron_ref.setY0(vertex_y_position, positionError_positionfit[1]);
				electron_ref.setZ0(vertex_z_position);
				auto const& electron_track_free_coeff = electron_ref.freeCoefficientsVector();
				auto const& electron_track_dir = electron_ref.directionVector();	


				for(auto const& h : electron_hits) {

					auto const& hit_module = h.module(); 
					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto indx = incoming_errors.GetNrows() + muon_errors.GetNrows() + hit_module.index();

					auto t_nominator = (mod_pos - electron_track_free_coeff).Dot(mod_norm);
					auto t_denominator = electron_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (electron_track_free_coeff + electron_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					Double_t dx_dxslope = (TVector3(t,0,0) + electron_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + electron_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dxslope * electron_track_dir[0] + dx_dyslope * electron_track_dir[1];

					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 4) =  dx_dxslope; //ax
					H(indx, 5) =  dx_dyslope; //ay
				}


				
				auto V = combined_errors;
				V.Invert();
				if(!V.IsValid())
					return 1;	

				TMatrixD HT = H;
				HT.T();

				TMatrixD HTVI = HT * V;

				TMatrixD cov = HTVI * H;
			
				cov.Invert();
				if(!cov.IsValid())
					return 1;		 

			
				auto HTVIX = HTVI * X;
				auto result = cov * HTVIX;


				auto XHR = X - H * result;
				auto chi2a = V * XHR;
				auto chi2 = XHR * chi2a;

				vertex.setChi2(chi2);


				vertex.setPositionKinematicFit(vertex_x_position, positionError_positionfit[0], vertex_y_position, positionError_positionfit[1], vertex_z_position, positionError_positionfit[2]);

				incoming_ref.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), vertex_x_position, positionError_positionfit[0]);
				incoming_ref.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), vertex_y_position, positionError_positionfit[1]);
				incoming_ref.setZ0(vertex_z_position);

				muon_ref.setXTrackParameters(result(2), TMath::Sqrt(cov(2,2)), vertex_x_position, positionError_positionfit[0]);
				muon_ref.setYTrackParameters(result(3), TMath::Sqrt(cov(3,3)), vertex_y_position, positionError_positionfit[1]);
				muon_ref.setZ0(vertex_z_position);

				electron_ref.setXTrackParameters(result(4), TMath::Sqrt(cov(4,4)), vertex_x_position, positionError_positionfit[0]);
				electron_ref.setYTrackParameters(result(5), TMath::Sqrt(cov(5,5)), vertex_y_position, positionError_positionfit[1]);
				electron_ref.setZ0(vertex_z_position);


				vertex.setLinearFitCovarianceMatrix(cov);

				vertex.recalculateAngles();	

			}
		}
		



	} else {

		//use KF to estimate XY position


		if(config.usePerpendicularDistanceFor3DTrackFit()) {


			//https://arxiv.org/pdf/hep-ex/9406006

			TVectorD X(combined_errors.GetNrows());
			X.Zero();

			TMatrixD H(combined_errors.GetNrows(), 8);
			H.Zero();
			//					0		1		2		3		4			5			6		7
			//A matrix order - ax_inc ay_inc ax_muon ay_muon ax_electron ay_electron xvertex yvertex
			
			//X - HA -> posperp - [(ax*(z-zvertex) + xvertex)*cosa + (ay*(z-zvertex) + yvertex)*sina]
			for(auto const& h : incoming_hits) {

				auto const& m = h.module();
				auto indx = m.index();

				X(indx) = h.positionPerpendicular();

				H(indx, 0) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 1) =  (h.z() - vertex_z_position) * m.angleSine();
				H(indx, 6) =  m.angleCosine();
				H(indx, 7) =  m.angleSine();	
			}


			for(auto const& h : muon_hits) {

				auto const& m = h.module();
				auto indx = incoming_errors.GetNrows() + m.index();

				X(indx) = h.positionPerpendicular();

				H(indx, 2) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 3) =  (h.z() - vertex_z_position) * m.angleSine();	
				H(indx, 6) =  m.angleCosine();
				H(indx, 7) =  m.angleSine();	
			}


			for(auto const& h : electron_hits) {

				auto const& m = h.module();
				auto indx = incoming_errors.GetNrows() + muon_errors.GetNrows() + m.index();

				X(indx) = h.positionPerpendicular();

				H(indx, 4) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 5) =  (h.z() - vertex_z_position) * m.angleSine();	
				H(indx, 6) =  m.angleCosine();
				H(indx, 7) =  m.angleSine();	
			}


			auto V = combined_errors;
			V.Invert();
			if(!V.IsValid())
				return 1;	

			TMatrixD HT = H;
			HT.T();

			TMatrixD HTVI = HT * V;

			TMatrixD cov = HTVI * H;
		
			cov.Invert();
			if(!cov.IsValid())
				return 1;		 

		
			auto HTVIX = HTVI * X;
			auto result = cov * HTVIX;

			auto XHR = X - H * result;
			auto chi2a = V * XHR;
			auto chi2 = XHR * chi2a;

			vertex.setChi2(chi2);

			
			vertex.setPositionKinematicFit(result(6), TMath::Sqrt(cov(6,6)), result(7), TMath::Sqrt(cov(7,7)), vertex_z_position, config.useFittedZPositionInKinematicFit() && position_fit_status? positionError_positionfit[2] : 0);

			incoming_ref.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), vertex_x_position, positionError_positionfit[0]);
			incoming_ref.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), vertex_y_position, positionError_positionfit[1]);
			incoming_ref.setZ0(vertex_z_position);

			muon_ref.setXTrackParameters(result(2), TMath::Sqrt(cov(2,2)), vertex_x_position, positionError_positionfit[0]);
			muon_ref.setYTrackParameters(result(3), TMath::Sqrt(cov(3,3)), vertex_y_position, positionError_positionfit[1]);
			muon_ref.setZ0(vertex_z_position);

			electron_ref.setXTrackParameters(result(4), TMath::Sqrt(cov(4,4)), vertex_x_position, positionError_positionfit[0]);
			electron_ref.setYTrackParameters(result(5), TMath::Sqrt(cov(5,5)), vertex_y_position, positionError_positionfit[1]);
			electron_ref.setZ0(vertex_z_position);

			vertex.setLinearFitCovarianceMatrix(cov);

			vertex.recalculateAngles();	


		} else {


			for(Int_t iter = 0; iter < config.numberOfIterationsInVertexFit(); ++iter) {

				TVectorD X(combined_errors.GetNrows());
				X.Zero();

				TMatrixD H(combined_errors.GetNrows(), 8);
				H.Zero();
				//					0		1		2		3		4			5			6		7
				//A matrix order - ax_inc ay_inc ax_muon ay_muon ax_electron ay_electron xvertex yvertex
				
				/*
					https://indico.cern.ch/event/1401448/contributions/5891071/attachments/2830668/4945820/updateAlignment_weekly_RP.pdf

					mod_pos = [xmod, ymod, zmod]
					mod_norm = [modnormx, modnormy, modnormz]
					mod_meas_dir = [moddirx, moddiry, moddirz]
					track_free_coeff = [x0, y0, z0] = [xvertex, yvertex, z0] -- z0 fixed
					track_dir = [xslope, yslope, 1]



					t = (mod_pos - track_free_coeff) . mod_norm / (track_dir . mod_norm)
					t = ( (xmod - x0) * modnormx + (ymod - y0) * modnormy + (zmod - z0) * modnormz) ) / (xslope * modnormx + yslope * modnormy + modnormz)

					dt/dx0 = -1 * modnormx / (track_dir . mod_norm)
					dt/dxslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormx / (track_dir . mod_norm)^2

					dt/dy0 = -1 * modnormy / (track_dir . mod_norm)
					dt/dyslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormy / (track_dir . mod_norm)^2


					x(p) = (track_free_coeff + track_dir * t - mod_pos) . mod_meas_dir
					x(p) = (x0 + xslope * t - xmod) * moddirx + (y0 + yslope * t - ymod) * moddiry + (z0 + t - zmod) * moddirz


					dx(p)/dxslope = ([1, 0, 0] * t + track_dir * dt/dxslope) . mod_meas_dir
					dx(p)/dyslope = ([0, 1, 0] * t + track_dir * dt/dyslope) . mod_meas_dir

					dx(p)/dxvertex = dx(p)/dx0 = ([1, 0, 0] + track_dir * dt/dx0) . mod_meas_dir
					dx(p)/dyvertex = dx(p)/dy0 = ([0, 1, 0] + track_dir * dt/dy0) . mod_meas_dir



					p = [ax_inc ay_inc ax_muon ay_muon ax_electron ay_electron xvertex yvertex] = A

					xlin(p) = x(p0) + gradx(p0) . (p - p0) = [x(p0) - gradx(p0) . p0] + [gradx(p0) . p] 

					chi2 = pos - xlin(p) = {pos - [x(p0) - gradx(p0) . p0]} - [gradx(p0) . p]
				*/		

				//X - HA -> pos - xlin(p) -> X = {pos - [x(p0) - gradx(p0) . p0]}; H = gradx(p0)


				//required for first iteration, they are equal for all subsequent ones
                Double_t averaged_x0 = (incoming_ref.x(vertex_z_position) + muon_ref.x(vertex_z_position) + electron_ref.x(vertex_z_position))/3;
				Double_t averaged_y0 = (incoming_ref.y(vertex_z_position) + muon_ref.y(vertex_z_position) + electron_ref.y(vertex_z_position))/3;


				incoming_ref.setX0(averaged_x0, 0);
				incoming_ref.setY0(averaged_y0, 0);
				incoming_ref.setZ0(vertex_z_position);
				auto const& incoming_track_free_coeff = incoming_ref.freeCoefficientsVector();
				auto const& incoming_track_dir = incoming_ref.directionVector();	

				for(auto const& h : incoming_hits) {

					auto const& hit_module = h.module(); 
					auto indx = hit_module.index();

					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto t_nominator = (mod_pos - incoming_track_free_coeff).Dot(mod_norm);
					auto t_denominator = incoming_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (incoming_track_free_coeff + incoming_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dx0 = -1 * mod_norm[0] / t_denominator;
					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dy0 = -1 * mod_norm[1] / t_denominator;
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					//gradx(p0)
					Double_t dx_dx0 = (TVector3(1,0,0) + incoming_track_dir * dt_dx0).Dot(mod_meas_dir);
					Double_t dx_dxslope = (TVector3(t,0,0) + incoming_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dy0 = (TVector3(0,1,0) + incoming_track_dir * dt_dy0).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + incoming_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dx0 * incoming_track_free_coeff[0] + dx_dxslope * incoming_track_dir[0] + dx_dy0 * incoming_track_free_coeff[1] + dx_dyslope * incoming_track_dir[1];


					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 0) =  dx_dxslope;
					H(indx, 1) =  dx_dyslope;
					H(indx, 6) =  dx_dx0;
					H(indx, 7) =  dx_dy0;	
				}


				muon_ref.setX0(averaged_x0, 0);
				muon_ref.setY0(averaged_y0, 0);
				muon_ref.setZ0(vertex_z_position);
				auto const& muon_track_free_coeff = muon_ref.freeCoefficientsVector();
				auto const& muon_track_dir = muon_ref.directionVector();	

				for(auto const& h : muon_hits) {

					auto const& hit_module = h.module(); 
					auto indx = incoming_errors.GetNrows() + hit_module.index();

					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto t_nominator = (mod_pos - muon_track_free_coeff).Dot(mod_norm);
					auto t_denominator = muon_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (muon_track_free_coeff + muon_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dx0 = -1 * mod_norm[0] / t_denominator;
					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dy0 = -1 * mod_norm[1] / t_denominator;
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					//gradx(p0)
					Double_t dx_dx0 = (TVector3(1,0,0) + muon_track_dir * dt_dx0).Dot(mod_meas_dir);
					Double_t dx_dxslope = (TVector3(t,0,0) + muon_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dy0 = (TVector3(0,1,0) + muon_track_dir * dt_dy0).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + muon_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dx0 * muon_track_free_coeff[0] + dx_dxslope * muon_track_dir[0] + dx_dy0 * muon_track_free_coeff[1] + dx_dyslope * muon_track_dir[1];



					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 2) =  dx_dxslope;
					H(indx, 3) =  dx_dyslope;	
					H(indx, 6) =  dx_dx0;
					H(indx, 7) =  dx_dy0;	
				}

				electron_ref.setX0(averaged_x0, 0);
				electron_ref.setY0(averaged_y0, 0);
				electron_ref.setZ0(vertex_z_position);
				auto const& electron_track_free_coeff = electron_ref.freeCoefficientsVector();
				auto const& electron_track_dir = electron_ref.directionVector();	

				for(auto const& h : electron_hits) {

					auto const& hit_module = h.module(); 
					auto indx = incoming_errors.GetNrows() + muon_errors.GetNrows() + hit_module.index();

					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto t_nominator = (mod_pos - electron_track_free_coeff).Dot(mod_norm);
					auto t_denominator = electron_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (electron_track_free_coeff + electron_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dx0 = -1 * mod_norm[0] / t_denominator;
					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dy0 = -1 * mod_norm[1] / t_denominator;
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					//gradx(p0)
					Double_t dx_dx0 = (TVector3(1,0,0) + electron_track_dir * dt_dx0).Dot(mod_meas_dir);
					Double_t dx_dxslope = (TVector3(t,0,0) + electron_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dy0 = (TVector3(0,1,0) + electron_track_dir * dt_dy0).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + electron_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dx0 * electron_track_free_coeff[0] + dx_dxslope * electron_track_dir[0] + dx_dy0 * electron_track_free_coeff[1] + dx_dyslope * electron_track_dir[1];



					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 4) =  dx_dxslope;
					H(indx, 5) =  dx_dyslope;	
					H(indx, 6) =  dx_dx0;
					H(indx, 7) =  dx_dy0;	
				}


				auto V = combined_errors;
				V.Invert();
				if(!V.IsValid())
					return 1;	

				TMatrixD HT = H;
				HT.T();

				TMatrixD HTVI = HT * V;

				TMatrixD cov = HTVI * H;
			
				cov.Invert();
				if(!cov.IsValid())
					return 1;		 

			
				auto HTVIX = HTVI * X;
				auto result = cov * HTVIX;

				auto XHR = X - H * result;
				auto chi2a = V * XHR;
				auto chi2 = XHR * chi2a;

				vertex.setChi2(chi2);

				
				vertex.setPositionKinematicFit(result(6), TMath::Sqrt(cov(6,6)), result(7), TMath::Sqrt(cov(7,7)), vertex_z_position, config.useFittedZPositionInKinematicFit() && position_fit_status? positionError_positionfit[2] : 0);

				incoming_ref.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), vertex_x_position, positionError_positionfit[0]);
				incoming_ref.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), vertex_y_position, positionError_positionfit[1]);
				incoming_ref.setZ0(vertex_z_position);

				muon_ref.setXTrackParameters(result(2), TMath::Sqrt(cov(2,2)), vertex_x_position, positionError_positionfit[0]);
				muon_ref.setYTrackParameters(result(3), TMath::Sqrt(cov(3,3)), vertex_y_position, positionError_positionfit[1]);
				muon_ref.setZ0(vertex_z_position);

				electron_ref.setXTrackParameters(result(4), TMath::Sqrt(cov(4,4)), vertex_x_position, positionError_positionfit[0]);
				electron_ref.setYTrackParameters(result(5), TMath::Sqrt(cov(5,5)), vertex_y_position, positionError_positionfit[1]);
				electron_ref.setZ0(vertex_z_position);

				vertex.setLinearFitCovarianceMatrix(cov);

				vertex.recalculateAngles();	

			}
		}


	}
	return 0;
}

template<Int_t N> Int_t MUonERecoFitter::addFitInfo(MUonERecoTarget const& target, std::vector<std::vector<MUonERecoModule>> const& modulesPerSector, MUonERecoGenericVertex& vertex, MUonEReconstructionConfiguration const& config) const {

	TVector3 position_positionfit; 
	TVector3 positionError_positionfit; 
	ROOT::Math::SMatrix<Double_t, 3> covarianceMatrix_positionfit;

	auto ot = vertex.outgoingTracks();
	ot.emplace_back(vertex.incomingTrack());

	Bool_t position_fit_status = fitVertexPosition(ot, position_positionfit, positionError_positionfit, covarianceMatrix_positionfit);

	vertex.setPositionFitStatus(position_fit_status);

	if(position_fit_status) {

		vertex.setPositionPositionFit(position_positionfit[0], positionError_positionfit[0], position_positionfit[1], positionError_positionfit[1], position_positionfit[2], positionError_positionfit[2]);
		vertex.setPositionFitCovarianceMatrix(covarianceMatrix_positionfit);
	
	} else {

		LOG(info) << "Position fit failed for a generic vertex. If it was to be used for the KF, middle of the target will be used instead";
	}

	auto vertex_z_position = target.z();
	Double_t vertex_x_position = -999;
	Double_t vertex_y_position = -999;

	if(!config.runKinematicFit()) {

		Double_t chi2 = vertex.incomingTrack().chi2();
		for(auto const& t : vertex.outgoingTracks()) {

			chi2 += t.chi2();
		}

		vertex.setChi2(chi2);
		return 0;
	}

	//thickness assumed for MS correction, default to half if position fitted during KF or independent position fit failed
	Double_t target_thickness_MS_incoming = 0.5 * (target.zMax() - target.zMin());
	Double_t target_thickness_MS_outgoing = target_thickness_MS_incoming;


	if(config.useFittedZPositionInGenericVertexKinematicFit() || config.useFittedPositionInGenericVertexKinematicFit()) {

		if(position_fit_status) {

			vertex_x_position = position_positionfit[0];
			vertex_y_position = position_positionfit[1];
			vertex_z_position = position_positionfit[2];
			
			if(config.restrictGenericVertexPositionToTarget()) {

				if(vertex_z_position < target.zMin())
					vertex_z_position = target.zMin();
				else if(vertex_z_position > target.zMax())
					vertex_z_position = target.zMax();

				if(vertex_x_position < target.xMin())
					vertex_x_position = target.xMin();
				else if(vertex_x_position > target.xMax())
					vertex_x_position = target.xMax();		

				if(vertex_y_position < target.yMin())
					vertex_y_position = target.yMin();
				else if(vertex_y_position > target.yMax())
					vertex_y_position = target.yMax();				
			}

			target_thickness_MS_incoming = vertex_z_position <= target.zMin() ? -1 : vertex_z_position - target.zMin();
			target_thickness_MS_outgoing = vertex_z_position >= target.zMax() ? -1 : target.zMax() - vertex_z_position;
		}
	}

	//not used if thickness is -1
	Double_t target_position_MS_incoming = target.zMin() + 0.5 * target_thickness_MS_incoming;
	Double_t target_position_MS_outgoing = target.zMax() - 0.5 * target_thickness_MS_outgoing;


	auto const& incoming_track = vertex.incomingTrack();
	auto const& outgoing_tracks = vertex.outgoingTracks();

	auto const& incoming_modules = modulesPerSector[incoming_track.sector()];
	auto const& outgoing_modules = modulesPerSector[outgoing_tracks.front().sector()];

	auto incoming_errors = calculateErrorMatrix(modulesPerSector[incoming_track.sector()], getHitZPositions<MUonERecoTrack3D>(incoming_track, incoming_track.hitsCopy(), incoming_modules), config.addBaselineMSCorrectionToAllTracks() ? config.MSCorrectionAssumedBeamEnergy() : -1, config.disableNonDiagonalCovarianceMatrixTerms(), config.addMSCorrectionFromTargetInKinematicFit() ? target_thickness_MS_incoming : -1, target_position_MS_incoming, target, true);
	
	Int_t error_matrix_size = incoming_errors.GetNrows() + outgoing_tracks.size() * outgoing_modules.size();
	TMatrixD combined_errors(error_matrix_size, error_matrix_size);
	combined_errors.Zero();

	TMatrixDSub(combined_errors, 0, incoming_errors.GetNrows() - 1, 0, incoming_errors.GetNrows() - 1) = incoming_errors;

	for(Int_t indx = 0; indx < outgoing_tracks.size(); ++indx) {

		auto outgoing_errors = calculateErrorMatrix(outgoing_modules, getHitZPositions<MUonERecoTrack3D>(outgoing_tracks[indx], outgoing_tracks[indx].hitsCopy(), outgoing_modules), config.addBaselineMSCorrectionToAllTracks() ? config.MSCorrectionAssumedBeamEnergy() : -1, config.disableNonDiagonalCovarianceMatrixTerms(), config.addMSCorrectionFromTargetInKinematicFit() ? target_thickness_MS_outgoing : -1, target_position_MS_outgoing, target, false);
		TMatrixDSub(combined_errors, incoming_errors.GetNrows() + indx * outgoing_errors.GetNrows(), incoming_errors.GetNrows() + (indx + 1) * outgoing_errors.GetNrows() - 1, incoming_errors.GetNrows() + indx * outgoing_errors.GetNrows(), incoming_errors.GetNrows() + (indx + 1) * outgoing_errors.GetNrows() - 1) = outgoing_errors;
	}
	

	if(config.useFittedPositionInGenericVertexKinematicFit() && position_fit_status) {


		if(config.usePerpendicularDistanceFor3DTrackFit()) {

			//https://arxiv.org/pdf/hep-ex/9406006

			TVectorD X(combined_errors.GetNrows());
			X.Zero();

			TMatrixD H(combined_errors.GetNrows(), 2 + 2 * outgoing_tracks.size());
			H.Zero();

			//A matrix order - ax_inc ay_inc ax_track1 ay_track1 ...
			
			//X - HA -> posperp - [(ax*(z-zvertex) + xvertex)*cosa + (ay*(z-zvertex) + yvertex)*sina]
			for(auto const& h : incoming_track.hitsCopy()) {

				auto const& m = h.module();
				auto indx = m.index();

				X(indx) = h.positionPerpendicular() - vertex_x_position * m.angleCosine() - vertex_y_position * m.angleSine();

				H(indx, 0) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 1) =  (h.z() - vertex_z_position) * m.angleSine();	
			}


			for(Int_t i = 0; i < outgoing_tracks.size(); ++i) {

				for(auto const& h : outgoing_tracks[i].hitsCopy()) {

					auto const& m = h.module();
					auto indx = incoming_modules.size() + i * outgoing_modules.size() + m.index();

					X(indx) = h.positionPerpendicular() - vertex_x_position * m.angleCosine() - vertex_y_position * m.angleSine();

					H(indx, 2 * (i + 1)) =  (h.z() - vertex_z_position) * m.angleCosine();
					H(indx, 2 * (i + 1) + 1) =  (h.z() - vertex_z_position) * m.angleSine();					
				}
			}


			
			auto V = combined_errors;
			V.Invert();
			if(!V.IsValid())
				return 1;	

			TMatrixD HT = H;
			HT.T();

			TMatrixD HTVI = HT * V;

			TMatrixD cov = HTVI * H;
		
			cov.Invert();
			if(!cov.IsValid())
				return 1;		 

		
			auto HTVIX = HTVI * X;
			auto result = cov * HTVIX;

			auto XHR = X - H * result;
			auto chi2a = V * XHR;
			auto chi2 = XHR * chi2a;

			vertex.setChi2(chi2);

			//x, x error, y, y error; z already set to target's z
			vertex.setPositionKinematicFit(vertex_x_position, positionError_positionfit[0], vertex_y_position, positionError_positionfit[1], vertex_z_position, positionError_positionfit[2]);

			auto& inc = vertex.incomingTrackRef();
			auto& out = vertex.outgoingTracksRef();

			inc.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), vertex_x_position, positionError_positionfit[0]);
			inc.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), vertex_y_position, positionError_positionfit[1]);
			inc.setZ0(vertex_z_position);

			for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {

				out[ot_index].setXTrackParameters(result(2 * (ot_index + 1)), TMath::Sqrt(cov(2 * (ot_index + 1),2 * (ot_index + 1))), vertex_x_position, positionError_positionfit[0]);
				out[ot_index].setYTrackParameters(result(2 * (ot_index + 1) + 1), TMath::Sqrt(cov(2 * (ot_index + 1) + 1,2 * (ot_index + 1) + 1)), vertex_y_position, positionError_positionfit[1]);
				out[ot_index].setZ0(vertex_z_position);
			}

			vertex.setLinearFitCovarianceMatrix(cov);


		} else {

			for(Int_t iter = 0; iter < config.numberOfIterationsInVertexFit(); ++iter) {

				TVectorD X(combined_errors.GetNrows());
				X.Zero();

				TMatrixD H(combined_errors.GetNrows(), 2 + 2 * outgoing_tracks.size());
				H.Zero();

				//A matrix order - ax_inc ay_inc ax_track1 ay_track1 ...
				
					/*
						https://indico.cern.ch/event/1401448/contributions/5891071/attachments/2830668/4945820/updateAlignment_weekly_RP.pdf

						mod_pos = [xmod, ymod, zmod]
						mod_norm = [modnormx, modnormy, modnormz]
						mod_meas_dir = [moddirx, moddiry, moddirz]
						track_free_coeff = [x0, y0, z0] -- fixed to full vertex position from position fit 
						track_dir = [xslope, yslope, 1]



						t = (mod_pos - track_free_coeff) . mod_norm / (track_dir . mod_norm)
						t = ( (xmod - x0) * modnormx + (ymod - y0) * modnormy + (zmod - z0) * modnormz) ) / (xslope * modnormx + yslope * modnormy + modnormz)

						dt/dxslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormx / (track_dir . mod_norm)^2
						dt/dyslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormy / (track_dir . mod_norm)^2


						x(p) = (track_free_coeff + track_dir * t - mod_pos) . mod_meas_dir
						x(p) = (x0 + xslope * t - xmod) * moddirx + (y0 + yslope * t - ymod) * moddiry + (z0 + t - zmod) * moddirz

						dx(p)/dxslope = ([1, 0, 0] * t + track_dir * dt/dxslope) . mod_meas_dir
						dx(p)/dyslope = ([0, 1, 0] * t + track_dir * dt/dyslope) . mod_meas_dir

						for a single track:

						p = [ax ay] = A

						xlin(p) = x(p0) + gradx(p0) . (p - p0) = [x(p0) - gradx(p0) . p0] + [gradx(p0) . p] 

						chi2 = pos - xlin(p) = {pos - [x(p0) - gradx(p0) . p0]} - [gradx(p0) . p]
					*/		

					//X - HA -> pos - xlin(p) -> X = {pos - [x(p0) - gradx(p0) . p0]}; H = gradx(p0)

				auto& incoming_ref = vertex.incomingTrackRef();
				auto& outgoing = vertex.outgoingTracksRef();

				incoming_ref.setX0(vertex_x_position, positionError_positionfit[0]);
				incoming_ref.setY0(vertex_y_position, positionError_positionfit[1]);
				incoming_ref.setZ0(vertex_z_position);
				auto const& incoming_track_free_coeff = incoming_ref.freeCoefficientsVector();
				auto const& incoming_track_dir = incoming_ref.directionVector();	


				for(auto const& h : incoming_track.hitsCopy()) {

					auto const& hit_module = h.module(); 
					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto indx = hit_module.index();

					auto t_nominator = (mod_pos - incoming_track_free_coeff).Dot(mod_norm);
					auto t_denominator = incoming_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (incoming_track_free_coeff + incoming_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					Double_t dx_dxslope = (TVector3(t,0,0) + incoming_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + incoming_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dxslope * incoming_track_dir[0] + dx_dyslope * incoming_track_dir[1];


					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 0) =  dx_dxslope; //ax
					H(indx, 1) =  dx_dyslope;	//ay
				}


				for(Int_t i = 0; i < outgoing_tracks.size(); ++i) {

					auto& out_track = outgoing[i];

					out_track.setX0(vertex_x_position, positionError_positionfit[0]);
					out_track.setY0(vertex_y_position, positionError_positionfit[1]);
					out_track.setZ0(vertex_z_position);
					auto const& out_track_free_coeff = out_track.freeCoefficientsVector();
					auto const& out_track_dir = out_track.directionVector();	



					for(auto const& h : outgoing_tracks[i].hitsCopy()) {

						auto const& hit_module = h.module(); 
						auto indx = incoming_modules.size() + i * outgoing_modules.size() + hit_module.index();

						auto const& mod_pos = hit_module.positionVector();
						auto const& mod_norm = hit_module.normalVector();
						auto const& mod_meas_dir = hit_module.measurementDirection();

						auto t_nominator = (mod_pos - out_track_free_coeff).Dot(mod_norm);
						auto t_denominator = out_track_dir.Dot(mod_norm);
						Double_t t = t_nominator / t_denominator;

						Double_t x_p0 = (out_track_free_coeff + out_track_dir * t - mod_pos).Dot(mod_meas_dir);

						Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
						Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

						Double_t dx_dxslope = (TVector3(t,0,0) + out_track_dir * dt_dxslope).Dot(mod_meas_dir);
						Double_t dx_dyslope = (TVector3(0,t,0) + out_track_dir * dt_dyslope).Dot(mod_meas_dir);

						Double_t gradx_dot_p0 = dx_dxslope * out_track_dir[0] + dx_dyslope * out_track_dir[1];

						X(indx) = h.position() - (x_p0 - gradx_dot_p0);

						H(indx, 2 * (i + 1)) =  dx_dxslope;
						H(indx, 2 * (i + 1) + 1) =  dx_dyslope;					
					}
				}


				
				auto V = combined_errors;
				V.Invert();
				if(!V.IsValid())
					return 1;	

				TMatrixD HT = H;
				HT.T();

				TMatrixD HTVI = HT * V;

				TMatrixD cov = HTVI * H;
			
				cov.Invert();
				if(!cov.IsValid())
					return 1;		 

			
				auto HTVIX = HTVI * X;
				auto result = cov * HTVIX;

				auto XHR = X - H * result;
				auto chi2a = V * XHR;
				auto chi2 = XHR * chi2a;

				vertex.setChi2(chi2);

				//x, x error, y, y error; z already set to target's z
				vertex.setPositionKinematicFit(vertex_x_position, positionError_positionfit[0], vertex_y_position, positionError_positionfit[1], vertex_z_position, positionError_positionfit[2]);



				incoming_ref.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), vertex_x_position, positionError_positionfit[0]);
				incoming_ref.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), vertex_y_position, positionError_positionfit[1]);
				incoming_ref.setZ0(vertex_z_position);

				for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {

					outgoing[ot_index].setXTrackParameters(result(2 * (ot_index + 1)), TMath::Sqrt(cov(2 * (ot_index + 1),2 * (ot_index + 1))), vertex_x_position, positionError_positionfit[0]);
					outgoing[ot_index].setYTrackParameters(result(2 * (ot_index + 1) + 1), TMath::Sqrt(cov(2 * (ot_index + 1) + 1,2 * (ot_index + 1) + 1)), vertex_y_position, positionError_positionfit[1]);
					outgoing[ot_index].setZ0(vertex_z_position);
				}

				vertex.setLinearFitCovarianceMatrix(cov);

			}

		}


	} else {

		//use KF to estimate XY position

		if(config.usePerpendicularDistanceFor3DTrackFit()) {

			//https://arxiv.org/pdf/hep-ex/9406006

			TVectorD X(combined_errors.GetNrows());
			X.Zero();

			TMatrixD H(combined_errors.GetNrows(), 2 + 2 * outgoing_tracks.size() + 2);
			H.Zero();


			//A matrix order - ax_inc ay_inc ax_track1 ay_track2 ... xvertex yvertex
			Int_t x_pos_index = 2 + 2 * outgoing_tracks.size();
			Int_t y_pos_index = 2 + 2 * outgoing_tracks.size() + 1;

			
			//X - HA -> posperp - [(ax*(z-zvertex) + xvertex)*cosa + (ay*(z-zvertex) + yvertex)*sina]
			for(auto const& h : incoming_track.hitsCopy()) {

				auto const& m = h.module();
				auto indx = m.index();

				X(indx) = h.positionPerpendicular();

				H(indx, 0) =  (h.z() - vertex_z_position) * m.angleCosine();
				H(indx, 1) =  (h.z() - vertex_z_position) * m.angleSine();
				H(indx, x_pos_index) =  m.angleCosine();
				H(indx, y_pos_index) =  m.angleSine();	
			}



			for(Int_t i = 0; i < outgoing_tracks.size(); ++i) {

				for(auto const& h : outgoing_tracks[i].hitsCopy()) {

					auto const& m = h.module();
					auto indx = incoming_modules.size() + i * outgoing_modules.size() + m.index();

					X(indx) = h.positionPerpendicular();

					H(indx, 2 * (i + 1)) =  (h.z() - vertex_z_position) * m.angleCosine();
					H(indx, 2 * (i + 1) + 1) =  (h.z() - vertex_z_position) * m.angleSine();	
					H(indx, x_pos_index) =  m.angleCosine();
					H(indx, y_pos_index) =  m.angleSine();					
				}
			}



			auto V = combined_errors;
			V.Invert();
			if(!V.IsValid())
				return 1;	

			TMatrixD HT = H;
			HT.T();

			TMatrixD HTVI = HT * V;

			TMatrixD cov = HTVI * H;
		
			cov.Invert();
			if(!cov.IsValid())
				return 1;		 

		
			auto HTVIX = HTVI * X;
			auto result = cov * HTVIX;

			auto XHR = X - H * result;
			auto chi2a = V * XHR;
			auto chi2 = XHR * chi2a;

			vertex.setChi2(chi2);
			
			//x, x error, y, y error;

			vertex.setPositionKinematicFit(result(x_pos_index), TMath::Sqrt(cov(x_pos_index, x_pos_index)), result(y_pos_index), TMath::Sqrt(cov(y_pos_index, y_pos_index)), vertex_z_position, config.useFittedZPositionInGenericVertexKinematicFit() && position_fit_status? positionError_positionfit[2] : 0);


			auto& inc = vertex.incomingTrackRef();
			auto& out = vertex.outgoingTracksRef();

			inc.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), result(x_pos_index), TMath::Sqrt(cov(x_pos_index, x_pos_index)));
			inc.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), result(y_pos_index), TMath::Sqrt(cov(y_pos_index, y_pos_index)));
			inc.setZ0(vertex_z_position);

			for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {


				out[ot_index].setXTrackParameters(result(2 * (ot_index + 1)), TMath::Sqrt(cov(2 * (ot_index + 1), 2 * (ot_index + 1))), result(x_pos_index), TMath::Sqrt(cov(x_pos_index, x_pos_index)));
				out[ot_index].setYTrackParameters(result(2 * (ot_index + 1) + 1), TMath::Sqrt(cov(2 * (ot_index + 1) + 1, 2 * (ot_index + 1) + 1)), result(y_pos_index), TMath::Sqrt(cov(y_pos_index, y_pos_index)));
				out[ot_index].setZ0(vertex_z_position);
			}


			vertex.setLinearFitCovarianceMatrix(cov);

		} else {

			for(Int_t iter = 0; iter < config.numberOfIterationsInVertexFit(); ++iter) {

				TVectorD X(combined_errors.GetNrows());
				X.Zero();

				TMatrixD H(combined_errors.GetNrows(), 2 + 2 * outgoing_tracks.size() + 2);
				H.Zero();


				//A matrix order - ax_inc ay_inc ax_track1 ay_track2 ... xvertex yvertex
				Int_t x_pos_index = 2 + 2 * outgoing_tracks.size();
				Int_t y_pos_index = 2 + 2 * outgoing_tracks.size() + 1;

				/*
					https://indico.cern.ch/event/1401448/contributions/5891071/attachments/2830668/4945820/updateAlignment_weekly_RP.pdf

					mod_pos = [xmod, ymod, zmod]
					mod_norm = [modnormx, modnormy, modnormz]
					mod_meas_dir = [moddirx, moddiry, moddirz]
					track_free_coeff = [x0, y0, z0] = [xvertex, yvertex, z0] -- z0 fixed
					track_dir = [xslope, yslope, 1]



					t = (mod_pos - track_free_coeff) . mod_norm / (track_dir . mod_norm)
					t = ( (xmod - x0) * modnormx + (ymod - y0) * modnormy + (zmod - z0) * modnormz) ) / (xslope * modnormx + yslope * modnormy + modnormz)

					dt/dx0 = -1 * modnormx / (track_dir . mod_norm)
					dt/dxslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormx / (track_dir . mod_norm)^2

					dt/dy0 = -1 * modnormy / (track_dir . mod_norm)
					dt/dyslope =  -1 * ( (mod_pos - track_free_coeff) . mod_norm ) * modnormy / (track_dir . mod_norm)^2


					x(p) = (track_free_coeff + track_dir * t - mod_pos) . mod_meas_dir
					x(p) = (x0 + xslope * t - xmod) * moddirx + (y0 + yslope * t - ymod) * moddiry + (z0 + t - zmod) * moddirz


					dx(p)/dxslope = ([1, 0, 0] * t + track_dir * dt/dxslope) . mod_meas_dir
					dx(p)/dyslope = ([0, 1, 0] * t + track_dir * dt/dyslope) . mod_meas_dir

					dx(p)/dxvertex = dx(p)/dx0 = ([1, 0, 0] + track_dir * dt/dx0) . mod_meas_dir
					dx(p)/dyvertex = dx(p)/dy0 = ([0, 1, 0] + track_dir * dt/dy0) . mod_meas_dir



					p = [ax_inc ay_inc ax_muon ay_muon ax_electron ay_electron xvertex yvertex] = A

					xlin(p) = x(p0) + gradx(p0) . (p - p0) = [x(p0) - gradx(p0) . p0] + [gradx(p0) . p] 

					chi2 = pos - xlin(p) = {pos - [x(p0) - gradx(p0) . p0]} - [gradx(p0) . p]
				*/		

				//X - HA -> pos - xlin(p) -> X = {pos - [x(p0) - gradx(p0) . p0]}; H = gradx(p0)


				auto& incoming_ref = vertex.incomingTrackRef();
				auto& outgoing = vertex.outgoingTracksRef();

				//required for first iteration, they are equal for all subsequent ones
				Double_t averaged_x0 = incoming_ref.x(vertex_z_position);
				Double_t averaged_y0 = incoming_ref.y(vertex_z_position);

				for(auto const& ot : outgoing_tracks) {

					averaged_x0 += ot.x(vertex_z_position);
					averaged_y0 += ot.y(vertex_z_position);
				}

				averaged_x0 /= outgoing_tracks.size() + 1;
				averaged_y0 /= outgoing_tracks.size() + 1;

				incoming_ref.setX0(averaged_x0, 0);
				incoming_ref.setY0(averaged_y0, 0);
				incoming_ref.setZ0(vertex_z_position);
				auto const& incoming_track_free_coeff = incoming_ref.freeCoefficientsVector();
				auto const& incoming_track_dir = incoming_ref.directionVector();	

				for(auto const& h : incoming_track.hitsCopy()) {

					auto const& hit_module = h.module(); 
					auto indx = hit_module.index();

					auto const& mod_pos = hit_module.positionVector();
					auto const& mod_norm = hit_module.normalVector();
					auto const& mod_meas_dir = hit_module.measurementDirection();

					auto t_nominator = (mod_pos - incoming_track_free_coeff).Dot(mod_norm);
					auto t_denominator = incoming_track_dir.Dot(mod_norm);
					Double_t t = t_nominator / t_denominator;

					Double_t x_p0 = (incoming_track_free_coeff + incoming_track_dir * t - mod_pos).Dot(mod_meas_dir);

					Double_t dt_dx0 = -1 * mod_norm[0] / t_denominator;
					Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
					Double_t dt_dy0 = -1 * mod_norm[1] / t_denominator;
					Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

					//gradx(p0)
					Double_t dx_dx0 = (TVector3(1,0,0) + incoming_track_dir * dt_dx0).Dot(mod_meas_dir);
					Double_t dx_dxslope = (TVector3(t,0,0) + incoming_track_dir * dt_dxslope).Dot(mod_meas_dir);
					Double_t dx_dy0 = (TVector3(0,1,0) + incoming_track_dir * dt_dy0).Dot(mod_meas_dir);
					Double_t dx_dyslope = (TVector3(0,t,0) + incoming_track_dir * dt_dyslope).Dot(mod_meas_dir);

					Double_t gradx_dot_p0 = dx_dx0 * incoming_track_free_coeff[0] + dx_dxslope * incoming_track_dir[0] + dx_dy0 * incoming_track_free_coeff[1] + dx_dyslope * incoming_track_dir[1];


					X(indx) = h.position() - (x_p0 - gradx_dot_p0);

					H(indx, 0) =  dx_dxslope;
					H(indx, 1) =  dx_dyslope;
					H(indx, x_pos_index) =  dx_dx0;
					H(indx, y_pos_index) =  dx_dy0;	
				}



				for(Int_t i = 0; i < outgoing_tracks.size(); ++i) {

					auto& out_track = outgoing[i];

					out_track.setX0(averaged_x0, 0);
					out_track.setY0(averaged_y0, 0);
					out_track.setZ0(vertex_z_position);
					auto const& out_track_free_coeff = out_track.freeCoefficientsVector();
					auto const& out_track_dir = out_track.directionVector();	


					for(auto const& h : outgoing_tracks[i].hitsCopy()) {

						auto const& hit_module = h.module(); 
						auto indx = incoming_modules.size() + i * outgoing_modules.size() + hit_module.index();

						auto const& mod_pos = hit_module.positionVector();
						auto const& mod_norm = hit_module.normalVector();
						auto const& mod_meas_dir = hit_module.measurementDirection();

						auto t_nominator = (mod_pos - out_track_free_coeff).Dot(mod_norm);
						auto t_denominator = out_track_dir.Dot(mod_norm);
						Double_t t = t_nominator / t_denominator;

						Double_t x_p0 = (out_track_free_coeff + out_track_dir * t - mod_pos).Dot(mod_meas_dir);

						Double_t dt_dx0 = -1 * mod_norm[0] / t_denominator;
						Double_t dt_dxslope = -1 * mod_norm[0] * t_nominator / (t_denominator * t_denominator);
						Double_t dt_dy0 = -1 * mod_norm[1] / t_denominator;
						Double_t dt_dyslope = -1 * mod_norm[1] * t_nominator / (t_denominator * t_denominator);

						//gradx(p0)
						Double_t dx_dx0 = (TVector3(1,0,0) + out_track_dir * dt_dx0).Dot(mod_meas_dir);
						Double_t dx_dxslope = (TVector3(t,0,0) + out_track_dir * dt_dxslope).Dot(mod_meas_dir);
						Double_t dx_dy0 = (TVector3(0,1,0) + out_track_dir * dt_dy0).Dot(mod_meas_dir);
						Double_t dx_dyslope = (TVector3(0,t,0) + out_track_dir * dt_dyslope).Dot(mod_meas_dir);

						Double_t gradx_dot_p0 = dx_dx0 * out_track_free_coeff[0] + dx_dxslope * out_track_dir[0] + dx_dy0 * out_track_free_coeff[1] + dx_dyslope * out_track_dir[1];


						X(indx) = h.position() - (x_p0 - gradx_dot_p0);

						H(indx, 2 * (i + 1)) =  dx_dxslope;
						H(indx, 2 * (i + 1) + 1) =  dx_dyslope;	
						H(indx, x_pos_index) =  dx_dx0;
						H(indx, y_pos_index) =  dx_dy0;					
					}
				}



				auto V = combined_errors;
				V.Invert();
				if(!V.IsValid())
					return 1;	

				TMatrixD HT = H;
				HT.T();

				TMatrixD HTVI = HT * V;

				TMatrixD cov = HTVI * H;
			
				cov.Invert();
				if(!cov.IsValid())
					return 1;		 

			
				auto HTVIX = HTVI * X;
				auto result = cov * HTVIX;

				auto XHR = X - H * result;
				auto chi2a = V * XHR;
				auto chi2 = XHR * chi2a;

				vertex.setChi2(chi2);
				
				//x, x error, y, y error;

				vertex.setPositionKinematicFit(result(x_pos_index), TMath::Sqrt(cov(x_pos_index, x_pos_index)), result(y_pos_index), TMath::Sqrt(cov(y_pos_index, y_pos_index)), vertex_z_position, config.useFittedZPositionInGenericVertexKinematicFit() && position_fit_status? positionError_positionfit[2] : 0);


				auto& inc = vertex.incomingTrackRef();
				auto& out = vertex.outgoingTracksRef();

				inc.setXTrackParameters(result(0), TMath::Sqrt(cov(0,0)), result(x_pos_index), TMath::Sqrt(cov(x_pos_index, x_pos_index)));
				inc.setYTrackParameters(result(1), TMath::Sqrt(cov(1,1)), result(y_pos_index), TMath::Sqrt(cov(y_pos_index, y_pos_index)));
				inc.setZ0(vertex_z_position);

				for(Int_t ot_index = 0; ot_index < outgoing_tracks.size(); ++ot_index) {


					out[ot_index].setXTrackParameters(result(2 * (ot_index + 1)), TMath::Sqrt(cov(2 * (ot_index + 1), 2 * (ot_index + 1))), result(x_pos_index), TMath::Sqrt(cov(x_pos_index, x_pos_index)));
					out[ot_index].setYTrackParameters(result(2 * (ot_index + 1) + 1), TMath::Sqrt(cov(2 * (ot_index + 1) + 1, 2 * (ot_index + 1) + 1)), result(y_pos_index), TMath::Sqrt(cov(y_pos_index, y_pos_index)));
					out[ot_index].setZ0(vertex_z_position);
				}


				vertex.setLinearFitCovarianceMatrix(cov);

			}
		}


	}
	return 0;

}



void MUonERecoFitter::addFirstStateInfo(MUonERecoTrack3D& track, Bool_t enableMSCorrection, Double_t momentum) const {
    std::vector<MUonERecoHit> hits = track.hitsCopy();
    ROOT::Math::SVector<Double_t,4> state_first, state_last;
    ROOT::Math::SMatrix<Double_t,4,4> cov_first, cov_last;

    // Run Kalman Filter backward to get first state 
    std::sort(hits.begin(), hits.end(), [](const MUonERecoHit &hitA, const MUonERecoHit &hitB) {return hitA.z() > hitB.z();});
    runKalmanFilter(hits, enableMSCorrection, momentum, state_first, cov_first); 

	// Run forward to get last state 
    std::sort(hits.begin(), hits.end(), [](const MUonERecoHit &hitA, const MUonERecoHit &hitB) {return hitA.z() < hitB.z();});
    runKalmanFilter(hits,  enableMSCorrection, momentum, state_last,  cov_last); 

    // save information
    track.setFirstState(true, hits.front().z(), state_first(0), state_first(1), state_first(2), state_first(3), cov_first);
    track.setLastState( true, hits.back().z(),  state_last(0),  state_last(1),  state_last(2),  state_last(3),  cov_last);
}

void MUonERecoFitter::runKalmanFilter(const std::vector<MUonERecoHit> &hits, Bool_t useMultipleScattering, Double_t momentum, ROOT::Math::SVector<Double_t,4> &outState, ROOT::Math::SMatrix<Double_t,4,4> &outCovariance) const {
    ROOT::Math::SVector<Double_t,4> state; 
    ROOT::Math::SMatrix<Double_t,4,4> covariance = I; 
    for (int i=0; i<4; i++) { // we start with no information on the state
        state(i)=0.0;
        covariance(i,i)=1e6;
    }

    double prevZ = hits.front().z();
    Double_t MS = 0;

    for (size_t i=0; i<hits.size(); i++) { // process Kalman Filter
        double z = hits[i].z();
        double dz = z - prevZ;
        prevZ = z;

        if(i>0) MS = hits[i-1].module().multipleScatteringContribution(momentum, 1); // MS from sensor on hit i-1 propagated to measurement i
        	// else the default value is already 0
        auto const& mod = hits[i].module();

        ROOT::Math::SMatrix<Double_t,1,4> projectionMatrix; 
	        projectionMatrix(0,0) = mod.angleCosine(); 
	        projectionMatrix(0,1) = mod.angleSine();
	        projectionMatrix(0,2) = 0.; 
	        projectionMatrix(0,3) = 0.;

        propagateKalmanState(state, covariance, dz, useMultipleScattering, MS);

        double estimatedMeasurement = (projectionMatrix * state)(0);   // it's a 1x1 matrix
        double realMeasurement = hits[i].positionPerpendicular();
        double measurementError = hits[i].positionError();
        double residual = realMeasurement - estimatedMeasurement;

        ROOT::Math::SMatrix<Double_t,4,1> kalmanGain;
        computeKalmanGain(covariance, projectionMatrix, measurementError, kalmanGain);
        updateKalmanState(state, covariance, kalmanGain, residual, projectionMatrix);
    }
    // add MS from last sensor since we want states after the last modules
    MS = hits[hits.size()-1].module().multipleScatteringContribution(momentum, 1);
    propagateKalmanState(state, covariance, 0, useMultipleScattering, MS);

    outState = state;
    outCovariance = covariance;
}

void MUonERecoFitter::propagateKalmanState(ROOT::Math::SVector<Double_t,4> &state, ROOT::Math::SMatrix<Double_t,4,4> &covariance, double dz, Bool_t useMultipleScattering, Double_t MS) const {
    ROOT::Math::SMatrix<Double_t, 4> transitionMatrix = I; // Renamed F to transitionMatrix
	    transitionMatrix(0,2) = dz;
	    transitionMatrix(1,3) = dz;

    state = transitionMatrix * state;
    covariance = transitionMatrix * covariance * ROOT::Math::Transpose(transitionMatrix);

    if (useMultipleScattering) {
        ROOT::Math::SMatrix<Double_t, 4, 4, ROOT::Math::MatRepSym<Double_t, 4>> processNoise;

        Double_t MS_squared = MS*MS;
        Double_t ds = MS_squared * dz;
        Double_t dd = ds * dz;

    	Double_t a[10] = {
            dd, 
            0 , dd, 
            ds, 0 , MS_squared, 
            0 , ds, 0, MS_squared
    	};

        processNoise.SetElements(a,a+10);
        covariance += processNoise;
    }
}

void MUonERecoFitter::computeKalmanGain(const ROOT::Math::SMatrix<Double_t,4,4> &covariance, const ROOT::Math::SMatrix<Double_t,1,4> &projectionMatrix, double measurementError, ROOT::Math::SMatrix<Double_t,4,1> &kalmanGain) const {
    double S = (projectionMatrix * covariance * ROOT::Math::Transpose(projectionMatrix))(0,0) + measurementError*measurementError;
    double S_Inverse = 1.0 / S;

    ROOT::Math::SMatrix<Double_t,4,1> CtHt = covariance * ROOT::Math::Transpose(projectionMatrix);

    for (int row=0; row<4; row++) {
        kalmanGain(row,0) = CtHt(row,0) * S_Inverse;
    }
}

void MUonERecoFitter::updateKalmanState(ROOT::Math::SVector<Double_t,4> &state, ROOT::Math::SMatrix<Double_t,4,4> &covariance, const ROOT::Math::SMatrix<Double_t,4,1> &kalmanGain, double residual, const ROOT::Math::SMatrix<Double_t,1,4> &projectionMatrix) const {
    for (int row=0; row<4; row++) {
        state(row) += kalmanGain(row,0) * residual;
    }
    covariance = (I - kalmanGain * projectionMatrix) * covariance;
}

Bool_t MUonERecoFitter::runAdaptiveFitter(MUonERecoTarget const& target, std::vector<MUonERecoTrack3D> const& tracks, std::vector<MUonERecoAdaptiveFitterVertex>& reconstrucedAFVertices, MUonEReconstructionConfiguration const& config, Bool_t runSeeding) const {

	std::vector<AFTrack> aftracks;
	aftracks.reserve(tracks.size());

	
	for(Int_t i = 0; i < tracks.size(); ++i) {

		auto const track = tracks[i];

		if(track.kalmanFitSuccessful()) {

			auto const slope_x = track.firstStateXSlope();
			auto const slope_y = track.firstStateYSlope();
			auto const x = track.firstStateX();
			auto const y = track.firstStateY();
			auto const z = track.firstStateZ();

			const Double_t dz = target.z() - z;

			aftracks.emplace_back(track, dz, i);
		}
	}

	//seeding
	std::vector<AFSeed> seeds;
	seeds.reserve(10);

	if(!runSeeding && aftracks.size() > 1) {

		seeds.emplace_back(target.z(), aftracks.begin(), aftracks.end());

	} else if (runSeeding) {

		std::vector<AFTrack>::iterator search_starting_point = aftracks.begin();
		
		do {

			//find two tracks with closest distance in xy plane, within reasonable limits
			Double_t lowest_distance = 999999;
			std::vector<AFTrack>::iterator first_track;
			std::vector<AFTrack>::iterator second_track;
			Bool_t seed_found = false;

			for(std::vector<AFTrack>::iterator first_track_it = search_starting_point; first_track_it < aftracks.end(); ++first_track_it) {
				for(std::vector<AFTrack>::iterator second_track_it = first_track_it + 1; second_track_it != aftracks.end(); ++second_track_it) {

					Double_t distance = first_track_it->distance(*second_track_it);

					if(distance < lowest_distance && distance < config.adaptiveFitterSeedWindow()) {

						seed_found = true;
						first_track = first_track_it;
						second_track = second_track_it;
						lowest_distance = distance;
					}
				}
			}

			//if seed is not found, no more seeds can be found anyway
			//so this can be an ending condition
			if(!seed_found) 
				break;

			//set both seed tracks as used
			first_track->markUsed();
			second_track->markUsed();

			//calculate midpoint between two tracks as reference for assigning tracks
			Double_t seed_x = 0.5 * (first_track->position()(0) + second_track->position()(0));
			Double_t seed_y = 0.5 * (first_track->position()(1) + second_track->position()(1));

			//seed tracks will start here
			std::vector<AFTrack>::iterator seed_starting_point = search_starting_point;


			//move seed tracks to the beginning of the range
			//starting point now points to the element after last used track, i.e. the two tracks in the seed
			search_starting_point = std::partition(search_starting_point, aftracks.end(), [](AFTrack const& track){return track.isUsed();});

			//search for other tracks that can be assigned to the seed among non-used tracks
			for(std::vector<AFTrack>::iterator it = search_starting_point; it != aftracks.end(); ++it) {

				if(it->distance(seed_x, seed_y) < config.adaptiveFitterSeedTrackWindow())
					it->markUsed();
			}


			//move assigned tracks to the beginning of the range, all seed tracks will now be contained in 
			//between seed_starting_point and the output of std::partition
			//at this point we're also guaranteed at least 2 tracks in seed

			//starting point for next seed search
			search_starting_point = std::partition(search_starting_point, aftracks.end(), [](AFTrack const& track){return track.isUsed();});
			
			seeds.emplace_back(target.z(), seed_starting_point, search_starting_point);

		} while(true);		
	}


	for(auto const& seed : seeds) {

		MUonERecoAdaptiveFitterVertex vertex;
		auto status = fitVertexAdaptive(seed, target, vertex, config);
		if(status)
			reconstrucedAFVertices.emplace_back(std::move(vertex));
	}

	return !reconstrucedAFVertices.empty();
}

Bool_t MUonERecoFitter::fitVertexAdaptive(const AFSeed& seed, MUonERecoTarget const& target, MUonERecoAdaptiveFitterVertex& reconstrucedAFVertex, MUonEReconstructionConfiguration const& config) const {

	std::vector<AFTrackInVertex> tracks(seed.begin(), seed.end());
	Bool_t converged = false;

	ROOT::Math::XYZPoint vertex_position(seed.x(), seed.y(), target.z());
	ROOT::Math::SMatrix<Double_t, 3> vertex_cov;

	Double_t chi2_total{0};
	Long_t selected_tracks_number{0};

	for(Long_t i = 0; i < config.adaptiveFitterMaxNumberOfIterations() && !converged; ++i) {

		ROOT::Math::SMatrix<Double_t, 3> halfD2_chi2_DX2;
		ROOT::Math::SVector<Double_t, 3> halfD_chi2_DX;

		chi2_total = 0;
		selected_tracks_number = 0;

		ROOT::Math::SVector<Double_t, 2> vertex_position2D{vertex_position.x(), vertex_position.y()};

		for(auto& track : tracks) {

			//chi2
			const Double_t dz = vertex_position.z() - track.z();

			ROOT::Math::SVector<Double_t, 2> res = vertex_position2D - (track.position() + dz * track.slopes());
			Double_t chi2 = ROOT::Math::Similarity(res, track.W());

			track.setWeight(0);
			if(chi2 < config.adaptiveFitterMaxTrackChi2()) {

				++selected_tracks_number;

				//Tukey's weight
				track.setWeight((1 - chi2 / config.adaptiveFitterMaxTrackChi2()) * (1 - chi2 / config.adaptiveFitterMaxTrackChi2()));
				halfD2_chi2_DX2 += track.weight() * track.HWH();
				halfD_chi2_DX += track.weight() * track.HW() * res;

				chi2_total += track.weight() * chi2;
			}
		}

		if(selected_tracks_number >= 2) {

			//compute new covariance
			vertex_cov = halfD2_chi2_DX2;
			vertex_cov.Invert();

			//diff wrt reference
			ROOT::Math::SVector<Double_t, 3> delta = -1 * vertex_cov * halfD_chi2_DX;

			chi2_total += ROOT::Math::Dot(delta, halfD_chi2_DX);

			//update position
			vertex_position.SetX(vertex_position.x() + delta(0));
			vertex_position.SetY(vertex_position.y() + delta(1));
			vertex_position.SetZ(vertex_position.z() + delta(2));

			converged = std::abs(delta(2)) < config.adaptiveFitterConvergenceMaxDeltaZ();

		} else
			break;

	} //end of iterations

	if(!converged)
		return false;

	std::vector<Int_t> track_ID;
	std::vector<Double_t> track_weights;
	track_ID.reserve(tracks.size());
	track_weights.reserve(tracks.size());

	for(auto const& track : tracks) {

		if(track.weight() > 0) {

			track_ID.push_back(track.trackIndex());
			track_weights.push_back(track.weight());
		}
	}

	if(track_ID.size() >= 2) {

		reconstrucedAFVertex = MUonERecoAdaptiveFitterVertex(vertex_position, vertex_cov, track_ID, track_weights, chi2_total, target.station(), target.index());
		return true;
	}	

	return false;
}


Bool_t MUonERecoFitter::fitVertexPosition(std::vector<MUonERecoTrack3D> const& tracks, TVector3& position, TVector3& positionError, ROOT::Math::SMatrix<Double_t, 3>& covarianceMatrix) const {

    /*
    point-track distance

    p0 = (x0, y0, z0)
    p = (x, y, z)
    n = (ax, ay, 1) normalized = (n1, n2, n3)

    d = |(p-p0) - ((p-p0).n)n|

    d = |(x-x0, y-y0, z-z0) - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * (n1, n2, n3)|

        d1 = x-x0 - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * n1
        d2 = y-y0 - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * n2
        d3 = z-z0 - (n1*(x-x0) + n2*(y-y0) + n3*(z-z0)) * n3

    chi^2 = d1^2 + d2^2 + d3^2
    */

   Double_t tmpA[6] = {0};
   Double_t tmpB[3] = {0};

   for(auto const& t : tracks) {

        auto x0 = t.xTrack().x0();
        auto y0 = t.yTrack().y0();
        auto z0 = t.xTrack().z0();

        auto n = t.directionVector().Unit();
        auto n1 = n.x();
        auto n2 = n.y();
        auto n3 = n.z();

        auto n1sq = n1 * n1;
        auto n2sq = n2 * n2;
        auto n3sq = n3 * n3;

        tmpA[0] += 2 * n1sq * n2sq + 2*n1sq*n3sq + (1-n1sq)*(2 - 2*n1sq);
        tmpA[1] += 2 * n1*n2*n3sq - 2*n1*n2*(1-n2sq) - n1*n2*(2-2*n1sq);
        tmpA[2] += 2*n1sq*n2sq + 2*n2sq*n3sq + (1-n2sq)*(2-2*n2sq);
        tmpA[3] += 2*n1*n2sq*n3 - 2*n1*n3*(1-n3sq) - n1*n3*(2-2*n1sq);
        tmpA[4] += 2*n1sq*n2*n3 - 2*n2*n3*(1-n3sq) - n2*n3*(2-2*n2sq);
        tmpA[5] += 2*n1sq*n3sq + 2*n2sq*n3sq + (1-n3sq)*(2-2*n3sq);

        tmpB[0] -= x0*(-2*n1sq*n1sq - 2*n1sq*n2sq - 2*n1sq*n3sq + 4*n1sq - 2) + y0*(-2*n1sq*n1*n2 - 2*n1*n2sq*n2 - 2*n1*n2*n3sq + 4*n1*n2) + z0*(-2*n1sq*n1*n3 - 2*n1*n2sq*n3 - 2*n1*n3sq*n3 + 4*n1*n3);
        tmpB[1] -= x0*(-2*n1sq*n1*n2 - 2*n1*n2sq*n2 - 2*n1*n2*n3sq + 4*n1*n2) + y0*(-2*n1sq*n2sq - 2*n2sq*n2sq - 2*n2sq*n3sq + 4*n2sq - 2) + z0*(-2*n1sq*n2*n3 - 2*n2sq*n2*n3 - 2*n2*n3sq*n3 + 4*n2*n3);
        tmpB[2] -= x0*(-2*n1sq*n1*n3 - 2*n1*n2sq*n3 - 2*n1*n3sq*n3 + 4*n1*n3) + y0*(-2*n1sq*n2*n3 - 2*n2sq*n2*n3 - 2*n2*n3sq*n3 + 4*n2*n3) + z0*(-2*n1sq*n3sq - 2*n2sq*n3sq - 2*n3sq*n3sq + 4*n3sq - 2);
   }

    ROOT::Math::SMatrix<Double_t, 3, 3, ROOT::Math::MatRepSym<Double_t, 3>> A(tmpA, 6);
    ROOT::Math::SVector<Double_t, 3> B(tmpB, 3);

	if(!A.Invert()) //A is now a covariance matrix
		return false; //failed to invert

    covarianceMatrix = A;

    auto c_svd = A * B;  

    position = TVector3(c_svd(0), c_svd(1), c_svd(2));  
    positionError = TVector3(TMath::Sqrt(A(0,0)), TMath::Sqrt(A(1,1)), TMath::Sqrt(A(2,2)));

    return true;

};



TMatrixD MUonERecoFitter::calculateErrorMatrix(std::vector<MUonERecoModule> const& modules, std::vector<Double_t> const& hit_z, Double_t p, Bool_t disableNonDiagonalTerms, Double_t thicknessFromTarget, Double_t targetPiecePosition, MUonERecoTarget const& target, Bool_t reverseWalk) const {

	//https://arxiv.org/pdf/hep-ex/9406006

	TMatrixD errors(modules.size(), modules.size());
	errors.Zero();


	if(p < 0) { //no MS correction, just position error

		for(Int_t i = 0; i < modules.size(); ++i) {

			errors(i, i) = modules[i].hitResolution() * modules[i].hitResolution();
		}

		return errors;
	}



	Double_t target_error_nodz = 0;
	Bool_t add_target = thicknessFromTarget > 0;

	if(add_target) {

		target_error_nodz = target.multipleScatteringContribution(p, 1, thicknessFromTarget);	
	}

	auto calc = [&](Int_t i, Int_t j) { //matrix indices

		//modules are already sorted by Z

		Double_t zi = hit_z[i];
		Double_t zj = hit_z[j];

		Double_t value = 0;

		if(add_target) {

			value += target_error_nodz * target_error_nodz * fabs(zi - targetPiecePosition) * fabs(zj - targetPiecePosition);
		}


		Int_t kmax = std::max(i, j);
		Int_t kmin = std::min(i, j);

		Double_t projection_factor = modules[kmin].angleCosine()*modules[kmax].angleCosine() + modules[kmin].angleSine()*modules[kmax].angleSine();

		if(reverseWalk) {

			for(int index = kmax + 1; index < modules.size(); ++index) {

				auto const& m = modules[index];
				auto z = hit_z[index];

				Double_t th0 = m.multipleScatteringContribution(p, 1);

				value += th0 * th0 * (z - zi) * (z - zj);
			} 

		} else {

			for(int index = 0; index < kmin; ++index) {

				auto const& m = modules[index];
				auto z = hit_z[index];

				Double_t th0 = m.multipleScatteringContribution(p, 1);

				value += th0 * th0 * (zi - z) * (zj - z);
			} 
		}

		return projection_factor * value;
	};

	if(disableNonDiagonalTerms) {

		for(Int_t i = 0; i < modules.size(); ++i) {

			errors(i, i) = calc(i, i) + modules[i].hitResolution() * modules[i].hitResolution();

		}
		
	} else {

		for(Int_t i = 0; i < modules.size(); ++i) {

			for(Int_t j = 0; j <= i; ++j) {

				//matrix is symmetric
				errors(i, j) = calc(i, j);
				if(i == j) {

					errors(i, j) += modules[i].hitResolution() * modules[i].hitResolution();

				} else {

					errors(j, i) = errors(i, j);
				}
			}
		}

	}

	return errors;
}

template<typename TrackType> std::vector<Double_t> MUonERecoFitter::getHitZPositions(TrackType const& track, std::vector<MUonERecoHit> const& hits, std::vector<MUonERecoModule> const& modules) const {

		std::vector<Double_t> z;
		std::vector<Bool_t> z_set;

		z.resize(modules.size(), -9999999999);
		z_set.resize(modules.size(), false);

		for(auto const& h : hits) {

			auto indx = h.moduleIndex();

			z[indx] = h.z();
			z_set[indx] = true;
		}

		//interpolate missing modules
		for(Int_t indx = 0; indx < z_set.size(); ++indx) {

			if(z_set[indx]) continue;

			z[indx] = track.findIntersectionWithModule(modules[indx]);
		}

		return z;	
}


template Int_t MUonERecoFitter::addFitInfo<3>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<4>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<5>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<6>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<7>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<8>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<9>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;
template Int_t MUonERecoFitter::addFitInfo<10>(MUonERecoTarget const&, std::vector<std::vector<MUonERecoModule>> const&, MUonERecoGenericVertex&, MUonEReconstructionConfiguration const&) const;

template std::vector<Double_t> MUonERecoFitter::getHitZPositions<MUonERecoTrack2D>(MUonERecoTrack2D const&, std::vector<MUonERecoHit> const&, std::vector<MUonERecoModule> const&) const;
template std::vector<Double_t> MUonERecoFitter::getHitZPositions<MUonERecoTrack3D>(MUonERecoTrack3D const&, std::vector<MUonERecoHit> const&, std::vector<MUonERecoModule> const&) const;


ClassImp(MUonERecoFitter)


