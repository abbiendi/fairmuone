#ifndef MUONERECOOUTPUTBASE_H
#define MUONERECOOUTPUTBASE_H

#include "MUonEReconstructionConfiguration.h"

class MUonERecoOutputBase {

public:

    MUonERecoOutputBase() = default;
    MUonERecoOutputBase(MUonEReconstructionConfiguration::OutputFormat of)
        : m_outputFormat(of) 
        {}
    virtual ~MUonERecoOutputBase() = default;

    MUonEReconstructionConfiguration::OutputFormat outputFormat() const {return m_outputFormat;}

private:

    MUonEReconstructionConfiguration::OutputFormat m_outputFormat;

};

#endif //MUONERECOOUTPUTBASE_H

