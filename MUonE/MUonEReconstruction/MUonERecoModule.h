#ifndef MUONERECOMODULE_H
#define MUONERECOMODULE_H

#include "Rtypes.h"
#include "TMath.h"
#include "TVector3.h"
#include <fairlogger/Logger.h>

#include "FairGeoLoader.h"
#include "FairGeoMedia.h"
#include "FairGeoInterface.h"
#include "FairGeoMedium.h"


#include "MUonEDetectorModuleGeometryConfiguration.h"
#include "MUonEDetectorStationModuleConfiguration.h"
#include <cmath>
#include <iostream>

/*
	Class used to store info on a single module of the tracking system in a unified way for all input types.
*/

class MUonERecoModule {

public:

	MUonERecoModule() = default;


	MUonERecoModule(Int_t index, Int_t sector, Int_t stationID, Int_t moduleID, Double_t z, Bool_t isMC, MUonEDetectorStationModuleConfiguration const& moduleConfig)
		: m_index(index), m_sector(sector), m_stationID(stationID), m_moduleID(moduleID),
		m_twoSensorsPerModule(moduleConfig.configuration().geometry().twoSensorsPerModule()),
		m_projection(moduleConfig.projection().name()), m_isStereo('u' == moduleConfig.projection().name() || 'v' == moduleConfig.projection().name()),
		m_z(z), m_sensorThickness(moduleConfig.configuration().geometry().sensorThickness()), m_hitResolution(moduleConfig.hitResolution()),
		m_negateHitPosition(isMC ? false : moduleConfig.projection().negateDataHitPosition()),
		m_tilt(moduleConfig.projection().tilt()),
		m_tiltSine(TMath::Sin(moduleConfig.projection().tilt() * TMath::DegToRad())),
		m_tiltCosine(TMath::Cos(moduleConfig.projection().tilt() * TMath::DegToRad())),
		m_angle(moduleConfig.projection().angle()),
		m_angleSine(TMath::Sin((moduleConfig.projection().angle()) * TMath::DegToRad())),
		m_angleCosine(TMath::Cos((moduleConfig.projection().angle()) * TMath::DegToRad())),
		m_gammaSine(0), m_gammaCosine(1),
		m_numberOfStrips(moduleConfig.configuration().geometry().numberOfStrips()), m_sensorSize(moduleConfig.configuration().geometry().sensorSizeMeasurementDirection()),
		m_distanceBetweenSensors(moduleConfig.configuration().geometry().distanceBetweenSensorCenters()),
		m_seedSensor(moduleConfig.configuration().digitization().seedSensor())
		{

			//get radiation length from G4 material
			static FairGeoLoader* geoLoad = FairGeoLoader::Instance();

			if(!geoLoad) //may happen if generation step isn't run
				geoLoad = new FairGeoLoader("TGeo", "Geo Loader");

			if(!geoLoad)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoLoader to get radiation length";

			static FairGeoInterface* geoFace = geoLoad->getGeoInterface();

			if(!geoFace)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoInterface to get radiation length";

			static FairGeoMedia* media = geoFace->getMedia();

			if(!media)
				LOG(fatal) << "Reconstruction: Failed to retrieve FairGeoMedia to get radiation length";

			FairGeoMedium* medium = media->getMedium(moduleConfig.configuration().geometry().sensorMaterial().c_str());		
			
			if(!medium) {//may happen if generation step isn't run

				//the name has to be hardcoded here for now, because FairRunAna does not allow to set it from run macro
				//copied from FairRunSim::SetMaterials
				    TString Mat = "";
				    TString work = getenv("GEOMPATH");
				    work.ReplaceAll("//", "/");
				    if (work.IsNull()) {
					work = getenv("VMCWORKDIR");
					Mat = work + "/geometry/";
					Mat.ReplaceAll("//", "/");
				    } else {
					Mat = work;
					if (!Mat.EndsWith("/")) {
					    Mat += "/";
					}
				    }
				    TString MatFname = Mat + "media.geo"; //media.geo hardcoded

				geoFace->setMediaFile(MatFname.Data());
				geoFace->readMedia();

				media = geoFace->getMedia();
				medium = media->getMedium(moduleConfig.configuration().geometry().sensorMaterial().c_str());
			}

			if(!medium)
				LOG(fatal) << "Reconstruction: Failed to retrieve module medium to get radiation length";
		
			m_radiationLength = medium->getRadiationLength();
			LOG(info) << "Reconstruction: Initializing module " << index << " with radiation length " << m_radiationLength;


			Double_t x0 = sensorThickness() / radiationLength();
			if(twoSensorsPerModule()) x0 *= 2;


			m_baseMSCorrection = 13.6 * TMath::Sqrt(x0) * (1.+0.038*TMath::Log(x0))/1000;

			recalculateDirectionalVectors();

		}


	Int_t index() const {return m_index;}
	Int_t sector() const {return m_sector;}

	Int_t stationID() const {return m_stationID;}
	Int_t moduleID() const {return m_moduleID;}

	Bool_t twoSensorsPerModule() const {return m_twoSensorsPerModule;}

	char projection() const {return m_projection;}

	Double_t z() const {return m_z + m_zOffset;}
	Double_t sensorThickness() const {return m_sensorThickness;}
	
	Double_t hitResolution() const {return m_hitResolution;}

	Double_t tiltSine() const {return m_tiltSine;}
	Double_t tiltCosine() const {return m_tiltCosine;}

	Double_t angleSine() const {return m_angleSine;}
	Double_t angleCosine() const {return m_angleCosine;}

	Double_t gammaSine() const {return m_gammaSine;}
	Double_t gammaCosine() const {return m_gammaCosine;}	

	TVector3 const& measurementDirection() const {return m_measurementDirection;}
	TVector3 const& stripDirection() const {return m_stripDirection;}
	TVector3 const& normalVector() const {return m_normalVector;}	
	TVector3 const& positionVector() const {return m_positionVector;}	

	Double_t xOffset() const {return m_xOffset;}
	Double_t yOffset() const {return m_yOffset;}
	Double_t zOffset() const {return m_zOffset;}
	Double_t angleOffset() const {return m_angleOffset;}
	Double_t tiltOffset() const {return m_tiltOffset;}
	Double_t gammaOffset() const {return m_gammaOffset;}

	Int_t numberOfStrips() const {return m_numberOfStrips;}
	Double_t sensorSize() const {return m_sensorSize;}
	Double_t distanceBetweenSensors() const {return m_distanceBetweenSensors;}
	Int_t seedSensor() const {return m_seedSensor;}

	Double_t radiationLength() const {return m_radiationLength;}
	Double_t multipleScatteringContribution(Double_t p, Int_t charge) const {return abs(charge) * m_baseMSCorrection / p;}

	void setAlignmentParameters(Double_t xOffset, Double_t yOffset, Double_t zOffset, Double_t angleOffset, Double_t tiltOffset, Double_t gammaOffset) {

		m_xOffset = xOffset;
		m_yOffset = yOffset;
		m_zOffset = zOffset;
		m_angleOffset = angleOffset;
		m_tiltOffset = tiltOffset;
		m_gammaOffset = gammaOffset;

		m_angleSine = TMath::Sin((m_angle + angleOffset) * TMath::DegToRad());
		m_angleCosine = TMath::Cos((m_angle + angleOffset) * TMath::DegToRad());

		m_tiltSine = TMath::Sin((m_tilt + tiltOffset) * TMath::DegToRad());
		m_tiltCosine = TMath::Cos((m_tilt + tiltOffset) * TMath::DegToRad());

		m_gammaSine = TMath::Sin(gammaOffset * TMath::DegToRad());
		m_gammaCosine = TMath::Cos(gammaOffset * TMath::DegToRad());	

		recalculateDirectionalVectors();
	}


	//convert stub information to position in cm)
	Double_t stubToPosition(Double_t seeding_center_strip, Double_t bend) const {

		if(m_negateHitPosition)
			return -1 * ((seeding_center_strip + 0.5 + 0.5 * bend) * m_sensorSize / m_numberOfStrips - 0.5 * m_sensorSize);
		else
			return (seeding_center_strip + 0.5 + 0.5 * bend) * m_sensorSize / m_numberOfStrips - 0.5 * m_sensorSize;
	}


	//calculate position in plane perpendicular to the Z axis (without tilt)
	//tilt is accounted for by the hit position (see MUonERecoHit.h)
	//this method is used to calculate local position of point on the 3D track
	Double_t localCoordinatePerpendicular(Double_t x, Double_t y) const {
		return x * m_angleCosine + y * m_angleSine;
	}

	Bool_t sameModuleAs(MUonERecoModule const& module) const {return sameSectorAs(module) && (m_index == module.index());}
	Bool_t sameSectorAs(MUonERecoModule const& module) const {return m_sector == module.sector();}
	Bool_t sameProjectionAs(MUonERecoModule const& module) const {return m_projection == module.projection();}
	Bool_t isStereo() const {return m_isStereo;}

	//return position along the beam axis (z) at which the line and module plane intersect 
	Double_t findIntersectionWithLine(TVector3 const& pointOnLine, TVector3 const& lineDirection) const {

		/*
			plane - Ax + By + Cz + D = 0 = n . p + D
			line - (x0, y0, z0) + k * (dx, dy, dz) = p0 + k * dl

			n . (p0 + k' * dl) + D = 0
			n . p0 + k' * (n . dl) + D = 0
			k' = (-D - n . p0) / (n . dl) = - (D + n . p0) / (n . dl)

			z = (p0 + k' . dl).z
		*/

		Double_t normal_dot_direction = m_normalVector.Dot(lineDirection);

		if(0 == normal_dot_direction) {//this should realistically never happen

			LOG(fatal) << "MUonERecoModule: Trying to calculate intersection point with track, but the track is parallel to the module";
			std::cout << "normal vector: (" + std::to_string(m_normalVector.x()) + ", " + std::to_string(m_normalVector.y()) + ", " + std::to_string(m_normalVector.z()) + ")" << std::endl;
			std::cout << "direction vector: (" + std::to_string(lineDirection.x()) + ", " + std::to_string(lineDirection.y()) + ", " + std::to_string(lineDirection.z()) + ")" << std::endl;
			return 99999999999999;
		}

		Double_t kprim = -1 * (m_planeConstantTerm + m_normalVector.Dot(pointOnLine)) / normal_dot_direction;
		TVector3 intersection = pointOnLine + kprim * lineDirection;

		return intersection.z();
	}

private:

	Int_t m_index{-1}; //index in m_modulesPerSector vector
	Int_t m_sector{-1}; //0-before 1st target, 1 after first, 2 after 2nd and so on

	//wrt. detector configuration
	Int_t m_stationID{-1};
	Int_t m_moduleID{-1};

	Bool_t m_twoSensorsPerModule{true};

	char m_projection{'x'};
	Bool_t m_isStereo{false};


	Double_t m_sensorThickness{0}; //for easy access in MS calculation

	Double_t m_hitResolution{0}; //for simplified geometry

	Bool_t m_negateHitPosition{false}; //can be turned on only when real data is processed


	//the actual values of tilt, angle and offsets are hidden
	//only sines and cosines are used outside

	Double_t m_tiltSine{0};
	Double_t m_tiltCosine{0};

	//angle around the Z axis
	Double_t m_angleSine{0}; //includes offset from alignment
	Double_t m_angleCosine{0};

	//angle around axis perp. to strips
	Double_t m_gammaSine{0};
	Double_t m_gammaCosine{0};

	//additional geometry info
	Int_t m_numberOfStrips{0};
	Double_t m_sensorSize{0};
	Double_t m_distanceBetweenSensors{0};
	Int_t m_seedSensor{-1};

	Double_t m_radiationLength{0};


	TVector3 m_measurementDirection;
	TVector3 m_stripDirection;
	TVector3 m_normalVector;
	TVector3 m_positionVector;

	//the free term of module plane equation, i.e. Ax + By + Cz + >D< = 0
	Double_t m_planeConstantTerm{0};

    ClassDef(MUonERecoModule, 2)


	void recalculateDirectionalVectors() {

		//https://indico.cern.ch/event/1401448/contributions/5891071/attachments/2830668/4945820/updateAlignment_weekly_RP.pdf
		//slide 17

		m_measurementDirection = TVector3(

			m_angleCosine * m_tiltCosine - m_angleSine * m_gammaSine * m_tiltSine,
			m_angleSine * m_tiltCosine + m_angleCosine * m_gammaSine * m_tiltSine,
			-1 * m_gammaCosine * m_tiltSine
		);

		m_stripDirection = TVector3(

			-1 * m_angleSine * m_gammaCosine,
			m_angleCosine * m_gammaCosine,
			m_gammaSine
		);

		m_normalVector = TVector3(

			m_angleCosine * m_tiltSine + m_angleSine * m_gammaSine * m_tiltCosine,
			m_angleSine * m_tiltSine - m_angleCosine * m_gammaSine * m_tiltCosine,
			m_gammaCosine * m_tiltCosine
		);

		m_positionVector = TVector3(xOffset(), yOffset(), z());

		m_planeConstantTerm = -1 * (m_normalVector.x() * m_xOffset + m_normalVector.y() * m_yOffset + m_normalVector.z() * m_z);
	}


protected: //for alignment module class

	Double_t m_z{0}; //z position of the module midpoint; only stored internally, returned value contains alignment to avoid confusion
	Double_t m_tilt{0}; //stored only internally to avoid confuson with alignment; 
	Double_t m_angle{0}; //stored only internally to avoid confuson with alignment; 

	Double_t m_xOffset{0};
	Double_t m_yOffset{0};
	Double_t m_zOffset{0};
	Double_t m_angleOffset{0};
	Double_t m_tiltOffset{0};
	Double_t m_gammaOffset{0};


	Double_t m_baseMSCorrection{0};
};


#endif //MUONERECOMODULE_H
