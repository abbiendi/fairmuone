#!/bin/sh

FILE=$1
COUNTER=$2
OUTPUTNAME=$3
INPUTFILE_NAME=$4
INPUTFILE_DIR=$5
ALIGNFILE_NAME=$6

PREVIOUS_COUNTER=$((COUNTER-=1))
COUNTER=$((COUNTER+=1))

if [ $COUNTER -eq 0 ]
then
cat << EOF > ${FILE}
outputFile: ${OUTPUTNAME}_recoForAlignment_it${COUNTER}.root

detectorConfiguration: TR2023_geometry_forAlignment

numberOfEvents: 500000

inputDirectory: $INPUTFILE_DIR
inputFiles: [${INPUTFILE_NAME}.root] #if inputDirectory set, use "*" for all files (note "", as * is yaml's special symbol)

saveParametersFile: false

runGeneration: false
runDigitization: false
runEventFilter: false

runReconstruction: true
reconstructionConfiguration:

  isMC: false
  verbose: false

  useSeedingSensorOnly: false #true #don't use bend

  runAdaptiveVertexFitter: false

  xyHitAssignmentWindow: 0.2 #cm
  uvHitAssignmentWindow: 0.3 #is the default #cm

  maxOutlierChi2: -1  #disables outlier hit removal during 3D track fitting (high chi2 due to no alignment)

  maxNumberOfSharedHits: -1  #strictly disables hit sharing between reconstructed tracks (but if I'm selecting events with only 1 stub/module probably this is not needed...)

  weightLinkedTracksByDepositedCharge: false
  allowTrivialIncomingTracks: false #allows for tracks without stereo hits; implemented for testrun

EOF


else
cat << EOF > ${FILE}
outputFile: ${OUTPUTNAME}_recoForAlignment_it${COUNTER}.root

detectorConfiguration: TR2023_geometry_forAlignment

numberOfEvents: 500000

inputDirectory: $INPUTFILE_DIR
inputFiles: [${INPUTFILE_NAME}.root] #if inputDirectory set, use "*" for all files (note "", as * is yaml's special symbol)

saveParametersFile: false

runGeneration: false
runDigitization: false
runEventFilter: false

runReconstruction: true
reconstructionConfiguration:

  isMC: false
  verbose: false

  useSeedingSensorOnly: false #true #don't use bend

  runAdaptiveVertexFitter: false

  xyHitAssignmentWindow: 0.2 #cm
  uvHitAssignmentWindow: 0.3 #is the default #cm

  alignmentFile: ${ALIGNFILE_NAME}_it${PREVIOUS_COUNTER}

  maxOutlierChi2: -1  #disables outlier hit removal during 3D track fitting (high chi2 due to no alignment)

  maxNumberOfSharedHits: -1  #strictly disables hit sharing between reconstructed tracks (but if I'm selecting events with only 1 stub/module probably this is not needed...)

  weightLinkedTracksByDepositedCharge: false
  allowTrivialIncomingTracks: false #allows for tracks without stereo hits; implemented for testrun

EOF
fi
