this directory contains the macros currently used for the iterative 12 modules alignment.

The idea is to run the track reconstruction and the simpleAlignmentJob iteratively for 10 times, in order to achieve convergence of the alignment parameters for all the modules.
`do_all_jobs.sh` is the main executable, which produces the yaml files needed for the reconstruction and alignment, and runs iteratively the jobs.
`makeRecoYaml.sh` produces the yaml file needed for the track reconstruction. The yaml file is produced in the directory `../../common/jobs/`, but this can be changed by changing lines 6 and 8 of `do_all_jobs-sh`.
`makeAlignYaml.sh` produces the yaml file needed for the alignment job. The yaml file is produced in the directory`../../common/jobs/`, but this can be changed by changing lines 6 and 9 of `do_all_jobs-sh`.


The reconstructionJob makes the reconstruction on 500k events. If you want to change it, change lines 20 and 60 in `makeRecoYaml.sh`.


The alignment file is produced in the default directory `../../common/alignment`.


Before launching the alignment procedure, you need to change some options related to the input and output files in `do_all_jobs.sh`:
1) line 13: INPUTFILE_NAME. It is the name of the decoded+merged root file that you want to use for the track reconstruction. Remove the `.root` extension from the filename

2) line 15: INPUTFILE_DIR. Path to the directory where the INPUTFILE_NAME is.

3) line 17: OUTPUTNAME. Name of the output root file from the track reconstruction. You can also specify the full path to the directory where the output file will be located. The iterative alignment programs will add the string `_recoForAlignment_it${COUNTER}.root` to the OUTPUTNAME that you choose, so there is no need to put the `.root` extension in your OUTPUTNAME. ${COUNTER} is used to keep track of which iteration is running.

4) line 19: ALIGNFILE_NAME. Name of the alignment file produced by the alignment job.



Once you changed the four settings above you are ready to launch the alignment procedure from this directory.
Usage:
bash do_all_jobs.sh

