#!/bin/sh

#set the environment
pwd
source ${PWD}/../../../bin/FairRootConfig.sh
JOBSDIR=${PWD}/../common/jobs/

YAML_RECO=${JOBSDIR}/trackReco_forAlignment.yaml
YAML_ALIGNMENT=${JOBSDIR}/simpleAlignment.yaml


#input file used for track reconstruction (can be any decoded file), required in input to the productionJob
INPUTFILE_NAME=beamMuons_forAlignment_600k_2cmTgtPosFromMetrologyGeometry_convertedToRealDataFormat
#directory where the input file is, required in input to the productionJob
INPUTFILE_DIR=${PWD}/simulationSamples/
#name of the file containing the output of the track reconstruction, required by the productionJob
OUTPUTNAME=${PWD}/simulationSamples/${INPUTFILE_NAME}
#name of the alingment file produced by the alignmentJob
ALIGNFILE_NAME=./alignParams_${INPUTFILE_NAME}


MAXITER=10
echo "total number of iterations: $MAXITER"


for (( ITER=0; ITER<=$MAXITER; ITER++ ))
do

	#update the yaml files
	source makeRecoYaml.sh ${YAML_RECO} $ITER $OUTPUTNAME $INPUTFILE_NAME $INPUTFILE_DIR $ALIGNFILE_NAME
	source makeAlignYaml.sh ${YAML_ALIGNMENT} $ITER $OUTPUTNAME $ALIGNFILE_NAME

	#run reconstruction
	root -l -b -q ../macros/runProductionJob.C'("'${YAML_RECO}'")'
	#run alignment job
	root -l -b -q ../macros/runAlignmentJob.C'("'${YAML_ALIGNMENT}'")'

done


#run last trackreco with the best alignment
LAST_ITER=$MAXITER
LAST_ITER=$((LAST_ITER+=1))
source makeRecoYaml.sh ${YAML_RECO} $LAST_ITER $OUTPUTNAME $INPUTFILE_NAME $INPUTFILE_DIR $ALIGNFILE_NAME
root -l -b -q ../macros/runProductionJob.C'("'${YAML_RECO}'")'




echo "Done."







