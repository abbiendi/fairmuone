#!/bin/sh

FILE=$1
COUNTER=$2
RECOFILE_NAME=$3
ALIGNFILE_NAME=$4

PREVIOUS_COUNTER=$((COUNTER-=1))
COUNTER=$((COUNTER+=1))

if [ $COUNTER -eq 0 ]
then
cat << EOF > ${FILE}
inputFile: ${RECOFILE_NAME}_recoForAlignment_it${COUNTER}.root 
detectorConfig: TR2023_geometry_forAlignment
numberOfEvents: -1        # if it is < 0 this means "all the events in the input file"
outputFile: ${ALIGNFILE_NAME}_it${COUNTER}
saveOutputInAlignmentDirectory: true

alignmentConfiguration:
  
  simplified: true
  maxTrackChi2PerNdf: -1    # if it is < 0 this means "no cuts"
  minNumberOfStereoHits: 4

  useSeedingSensorOnly: false

  #align only module coordinate and alpha
  alignXY:    true
  alignZ:     false  # true/false: also try/don't try to align the longitudinal direction
  alignAngle: true
  alignTilt:  false

  freezePerpendicularOffsetForXYModules: true
 
EOF

else
cat << EOF > ${FILE}
inputFile: ${RECOFILE_NAME}_recoForAlignment_it${COUNTER}.root 
detectorConfig: TR2023_geometry_forAlignment
numberOfEvents: -1        # if it is < 0 this means "all the events in the input file"
outputFile: ${ALIGNFILE_NAME}_it${COUNTER}
saveOutputInAlignmentDirectory: true

alignmentConfiguration:

  simplified: true
  maxTrackChi2PerNdf: -1    # if it is < 0 this means "no cuts"
  minNumberOfStereoHits: 4

  useSeedingSensorOnly: false

  #align only module coordinate and alpha
  alignXY:    true
  alignZ:     false  # true/false: also try/don't try to align the longitudinal direction
  alignAngle: true
  alignTilt:  false

  freezePerpendicularOffsetForXYModules: true

  initialAlignmentFile: ${ALIGNFILE_NAME}_it${PREVIOUS_COUNTER}
EOF
fi
