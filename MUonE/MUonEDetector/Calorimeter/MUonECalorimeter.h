#ifndef MUONECALORIMETER_H
#define MUONECALORIMETER_H

/*
    Calorimeter detector class. Responsible for registering geometry and simulating response.
    Implementing only bare minimum. Additional methods of FairDetector may be useful in the future.
*/


#include "FairDetector.h"
#include "TLorentzVector.h"

#include <unordered_map>
#include <utility>

#include "MUonEDetectorConfiguration.h"
#include "MUonECalorimeterPoint.h"


class FairVolume;
class TClonesArray;

class MUonECalorimeter : public FairDetector {

public:

    MUonECalorimeter();
    MUonECalorimeter(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveCalorimeterPoints = true);

    virtual ~MUonECalorimeter() override;

    void setConfiguration(MUonEDetectorConfiguration const& detectorConfiguration, Bool_t saveCalorimeterPoints = true);


    //registers output containers
	virtual void   Register() override;

    //returns containers
	virtual TClonesArray* GetCollection(Int_t iColl) const override;

    //register detector geometry
    virtual void ConstructGeometry() override;

    //called at each step through the volume
    virtual Bool_t ProcessHits(FairVolume *v=0) override;

    //resets containers after each event
    virtual void   Reset() override;

    //cleanup
    virtual void EndOfEvent() override;

private:

    //detector information
    MUonEDetectorConfiguration m_detectorConfiguration;
    Bool_t m_saveCalorimeterPoints{true};

    //output container
    TClonesArray* m_calorimeterPoints{nullptr};

    //maps crystal copy id to (column, row) id pair
    std::unordered_map<Int_t, std::pair<Int_t, Int_t>> m_idMap;

	//temporary information stored between 
    //processHits calls for a single track
	TVector3 m_trackEnteringPositionGlobalCoordinates;
	TVector3 m_trackEnteringPositionLocalCoordinates;
	Double32_t m_trackTotalEnergyLoss{0};    

    //initialize materials
	Int_t InitMedium(const char* name);


	ClassDef(MUonECalorimeter, 2)

};


#endif //MUONECALORIMETER_H

