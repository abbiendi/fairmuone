#include "MUonEDetectorStationModuleConfiguration.h"

#include <fairlogger/Logger.h>

MUonEDetectorStationModuleConfiguration::MUonEDetectorStationModuleConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorStationModuleConfiguration::MUonEDetectorStationModuleConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration) {

    readConfiguration(config, projections, baseConfiguration);
}

Bool_t MUonEDetectorStationModuleConfiguration::readConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration) {


    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    m_configuration = baseConfiguration;
    m_configuration.updateConfiguration(config);

    if(config["projection"]) {

        auto tmp_proj = config["projection"].as<char>();

        try {

            m_projection = projections.at(tmp_proj);

        } catch(...) {

            LOG(error) << "Projection set to " << tmp_proj << ", but it's not defined in the modulesConfiguration/projections section.";
            m_configuredCorrectly = false;
        }
    }
    else {

        LOG(error) << "Variable 'projection' not set.";
        m_configuredCorrectly = false;
    }

    if(config["relativePosition"])
        m_relativePosition = config["relativePosition"].as<Double_t>();
    else {

        LOG(error) << "Variable 'relativePosition' not set.";
        m_configuredCorrectly = false;
    }

    if(config["hitResolution"])
        m_hitResolution = config["hitResolution"].as<Double_t>();
    else {

        LOG(error) << "Variable 'hitResolution' not set.";
        m_configuredCorrectly = false;
    }

    if(!m_configuredCorrectly)
        LOG(info) << "One of the modules in detector/stations/modules section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorStationModuleConfiguration::logCurrentConfiguration() const {

    LOG(info) << "projection: " << m_projection.name();
    LOG(info) << "hitResolution: " << m_hitResolution << " cm";
    LOG(info) << "relativePosition: " << m_relativePosition << " cm";
    LOG(info) << "configuration:";
    m_configuration.logCurrentConfiguration();
}

void MUonEDetectorStationModuleConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_configuration.resetDefaultConfiguration();
    m_projection.resetDefaultConfiguration();
    m_relativePosition = 0;
    m_hitResolution = 0;
}

ClassImp(MUonEDetectorStationModuleConfiguration)

