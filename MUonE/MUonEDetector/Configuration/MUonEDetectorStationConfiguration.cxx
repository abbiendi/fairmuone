#include "MUonEDetectorStationConfiguration.h"

#include <fairlogger/Logger.h>

MUonEDetectorStationConfiguration::MUonEDetectorStationConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorStationConfiguration::MUonEDetectorStationConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration, MUonEDetectorTargetsConfiguration const& targetBaseConfiguration) {

    readConfiguration(config, projections, baseConfiguration, targetBaseConfiguration);
}

Bool_t MUonEDetectorStationConfiguration::readConfiguration(YAML::Node const& config, std::unordered_map<char, MUonEDetectorProjectionConfiguration> const& projections, MUonEDetectorModulesConfiguration const& baseConfiguration, MUonEDetectorTargetsConfiguration const& targetBaseConfiguration) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["origin"])
        m_origin = config["origin"].as<Double_t>();
    else {

        LOG(error) << "Variable 'origin' not set.";
        m_configuredCorrectly = false;
    }

    if(config["targetRelativePosition"]) {

        m_hasTarget = true;
        m_targetRelativePosition = config["targetRelativePosition"].as<Double_t>();

        m_targetConfiguration = targetBaseConfiguration;

        if(config["targetConfiguration"])
            m_targetConfiguration.updateConfiguration(config["targetConfiguration"]);
    }


    if(config["modules"]) {

        m_modules.reserve(10);
        for(auto const& mod : config["modules"]) {

            auto tmp_mod = MUonEDetectorStationModuleConfiguration();
            if(!tmp_mod.readConfiguration(mod, projections, baseConfiguration))
                m_configuredCorrectly = false;
            else
                m_modules.emplace_back(tmp_mod);
        }

        //sort modules by their relative position
        std::sort(m_modules.begin(), m_modules.end(), [](MUonEDetectorStationModuleConfiguration const& lhs, MUonEDetectorStationModuleConfiguration const& rhs){return lhs.relativePosition() < rhs.relativePosition();});
    }


    if(!m_configuredCorrectly)
        LOG(info) << "One of the stations in detector/stations section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorStationConfiguration::logCurrentConfiguration() const {

    LOG(info) << "origin: " << m_origin << " cm";

    if(m_hasTarget){
        LOG(info) << "targetRelativePosition: " << m_targetRelativePosition << " cm";
        LOG(info) << "absolute: " << targetPosition() << " cm";
        m_targetConfiguration.logCurrentConfiguration();

    } else 
        LOG(info) << "Station has no target.";
    
    if(!m_modules.empty()) {
        LOG(info) << "";
        LOG(info) << "Station has " << m_modules.size() << " modules:";
        LOG(info) << "";
        for(Int_t module_index = 0; module_index < m_modules.size(); ++module_index) {
            LOG(info) << "Configuration of module " << module_index << ":";
            m_modules[module_index].logCurrentConfiguration();
            LOG(info) << "absolute position: " << modulePosition(m_modules[module_index]) << " cm";
            LOG(info) << "";
        }
    } else
        LOG(info) << "Station has no modules.";
}

void MUonEDetectorStationConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_origin = 0;

    m_hasTarget = false;    
    m_targetRelativePosition = 0;

    m_modules.clear();
}

ClassImp(MUonEDetectorStationConfiguration)

