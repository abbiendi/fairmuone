#include "MUonEDetectorCalorimeterGeometryConfiguration.h"


#include <fairlogger/Logger.h>

MUonEDetectorCalorimeterGeometryConfiguration::MUonEDetectorCalorimeterGeometryConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorCalorimeterGeometryConfiguration::MUonEDetectorCalorimeterGeometryConfiguration(YAML::Node const& config) {

    readConfiguration(config);
}

Bool_t MUonEDetectorCalorimeterGeometryConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["crystalMaterial"])
        m_crystalMaterial = config["crystalMaterial"].as<std::string>();
    else {

        LOG(error) << "Variable 'crystalMaterial' not set.";
        m_configuredCorrectly = false;
    }

    if(config["crystalWidthUpstream"])
        m_crystalWidthUpstream = config["crystalWidthUpstream"].as<Double_t>();
    else {

        LOG(error) << "Variable 'crystalWidthUpstream' not set.";
        m_configuredCorrectly = false;
    }

    if(config["crystalHeightUpstream"])
        m_crystalHeightUpstream = config["crystalHeightUpstream"].as<Double_t>();
    else {

        LOG(error) << "Variable 'crystalHeightUpstream' not set.";
        m_configuredCorrectly = false;
    }

    if(config["crystalWidthDownstream"])
        m_crystalWidthDownstream = config["crystalWidthDownstream"].as<Double_t>();
    else {

        LOG(error) << "Variable 'crystalWidthDownstream' not set.";
        m_configuredCorrectly = false;
    }

    if(config["crystalHeightDownstream"])
        m_crystalHeightDownstream = config["crystalHeightDownstream"].as<Double_t>();
    else {

        LOG(error) << "Variable 'crystalHeightDownstream' not set.";
        m_configuredCorrectly = false;
    }

    if(config["crystalThickness"])
        m_crystalThickness = config["crystalThickness"].as<Double_t>();
    else {

        LOG(error) << "Variable 'crystalThickness' not set.";
        m_configuredCorrectly = false;
    }

    if(config["numberOfColumns"])
        m_numberOfColumns = config["numberOfColumns"].as<Int_t>();
    else {

        LOG(error) << "Variable 'numberOfColumns' not set.";
        m_configuredCorrectly = false;
    }

    if(config["numberOfRows"])
        m_numberOfRows = config["numberOfRows"].as<Int_t>();
    else {

        LOG(error) << "Variable 'numberOfRows' not set.";
        m_configuredCorrectly = false;
    }

    if(config["distanceBetweenCrystalCenters"])
        m_distanceBetweenCrystalCenters = config["distanceBetweenCrystalCenters"].as<Double_t>();
    else {

        LOG(error) << "Variable 'distanceBetweenCrystalCenters' not set.";
        m_configuredCorrectly = false;
    }

    if(!m_configuredCorrectly)
        LOG(info) << "calorimeterConfiguration/geometry section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorCalorimeterGeometryConfiguration::logCurrentConfiguration() const {

    LOG(info) << "Calorimeter geometry configuration:";

    LOG(info) << "crystalMaterial: " << m_crystalMaterial;
    LOG(info) << "crystalWidthUpstream: " << m_crystalWidthUpstream << " cm";
    LOG(info) << "crystalHeightUpstream: " << m_crystalHeightUpstream << " cm";
    LOG(info) << "crystalWidthDownstream: " << m_crystalWidthDownstream << " cm";
    LOG(info) << "crystalHeightDownstream: " << m_crystalHeightDownstream << " cm";
    LOG(info) << "crystalThickness: " << m_crystalThickness << " cm";
    LOG(info) << "numberOfColumns: " << m_numberOfColumns;
    LOG(info) << "numberOfRows: " << m_numberOfRows;
    LOG(info) << "distanceBetweenCrystalCenters: " << m_distanceBetweenCrystalCenters << " cm";

}

void MUonEDetectorCalorimeterGeometryConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_crystalMaterial = "";

    m_crystalWidthUpstream = 0;
    m_crystalHeightUpstream = 0;

    m_crystalWidthDownstream = 0;
    m_crystalHeightDownstream = 0;

    m_crystalThickness = 0;

    m_numberOfColumns = 0;
    m_numberOfRows = 0;

    m_distanceBetweenCrystalCenters = 0;
}

ClassImp(MUonEDetectorCalorimeterGeometryConfiguration)
