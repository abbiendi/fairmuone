#ifndef MUONEDETECTORCALORIMETERDIGITIZATIONCONFIGURATION_H
#define MUONEDETECTORCALORIMETERDIGITIZATIONCONFIGURATION_H


#include "Rtypes.h"

#include <yaml-cpp/yaml.h>

#include <string>


class MUonEDetectorCalorimeterDigitizationConfiguration {


public:

    MUonEDetectorCalorimeterDigitizationConfiguration();
    MUonEDetectorCalorimeterDigitizationConfiguration(YAML::Node const& config);

    Bool_t readConfiguration(YAML::Node const& config);
    void logCurrentConfiguration() const;


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Double_t resolution() const {return m_resolution;}

    void resetDefaultConfiguration();

private:

    Bool_t m_configuredCorrectly{false};

    Double_t m_resolution{0};

	ClassDef(MUonEDetectorCalorimeterDigitizationConfiguration, 2)

};

#endif //MUONEDETECTORCALORIMETERDIGITIZATIONCONFIGURATION_H

