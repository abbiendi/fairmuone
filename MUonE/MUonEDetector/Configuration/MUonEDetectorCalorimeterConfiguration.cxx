#include "MUonEDetectorCalorimeterConfiguration.h"

#include <fairlogger/Logger.h>

MUonEDetectorCalorimeterConfiguration::MUonEDetectorCalorimeterConfiguration() {

    resetDefaultConfiguration();
}

MUonEDetectorCalorimeterConfiguration::MUonEDetectorCalorimeterConfiguration(YAML::Node const& config) {

    readConfiguration(config);
}

Bool_t MUonEDetectorCalorimeterConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();

    m_configuredCorrectly = true;

    if(config["geometry"]) {

        if(!m_geometry.readConfiguration(config["geometry"]))
            m_configuredCorrectly = false;
        else
            m_hasGeometry = true;
    }
    else {

        m_hasGeometry = false;
        LOG(info) << "'geometry' section not found.";
    }

    if(config["digitization"]) {

        if(!m_digitization.readConfiguration(config["digitization"]))
            m_configuredCorrectly = false;
        else
            m_hasDigitization = true;
    }
    else {

        m_hasDigitization = false;
        LOG(info) << "'digitization' section not found.";
    }

    if(!m_configuredCorrectly)
        LOG(info) << "calorimeterConfiguration section contains errors. Please fix them and try again.";

    return m_configuredCorrectly;
}

void MUonEDetectorCalorimeterConfiguration::logCurrentConfiguration() const {


    LOG(info) << "";

    if(m_hasGeometry)
        m_geometry.logCurrentConfiguration();
    else
        LOG(info) << "Calorimeter geometry configuration not defined.";

    LOG(info) << "";

    if(m_hasDigitization)
        m_digitization.logCurrentConfiguration();
    else
        LOG(info) << "Calorimeter digitization configuration not defined.";

}

void MUonEDetectorCalorimeterConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;
    m_geometry.resetDefaultConfiguration();

    m_hasGeometry = false;
    m_hasDigitization = false;
}

ClassImp(MUonEDetectorCalorimeterConfiguration)
