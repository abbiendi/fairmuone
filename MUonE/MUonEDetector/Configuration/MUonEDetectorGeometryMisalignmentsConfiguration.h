#ifndef MUONEDETECTORGEOMETRYMISALIGNMENTSCONFIGURATION_H
#define MUONEDETECTORGEOMETRYMISALIGNMENTSCONFIGURATION_H

#include "Rtypes.h"
#include "TString.h"

#include <string>
#include <vector>
#include <yaml-cpp/yaml.h>


class MUonEDetectorGeometryMisalignmentsConfiguration {

public:

    MUonEDetectorGeometryMisalignmentsConfiguration();
    MUonEDetectorGeometryMisalignmentsConfiguration(TString config_name);

    Bool_t readConfiguration(TString config_name);
    void logCurrentConfiguration() const;


    Double_t xGeometryOffset(int stationID, int moduleID) const {return m_xGeometryOffset[stationID][moduleID];}//cm
    Double_t yGeometryOffset(int stationID, int moduleID) const {return m_yGeometryOffset[stationID][moduleID];}//cm
    Double_t zGeometryOffset(int stationID, int moduleID) const {return m_zGeometryOffset[stationID][moduleID];}//cm

    Double_t angleGeometryOffset(int stationID, int moduleID) const {return m_angleGeometryOffset[stationID][moduleID];}//deg
    Double_t tiltGeometryOffset(int stationID, int moduleID)  const {return m_tiltGeometryOffset[stationID][moduleID]; }//deg
    Double_t gammaGeometryOffset(int stationID, int moduleID) const {return m_gammaGeometryOffset[stationID][moduleID];}//deg

    void setIsIdealGeometry();
    Bool_t isIdealGeometry() const {return m_isIdealGeometry;}

    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};
    Bool_t m_isIdealGeometry{true};

    std::vector<std::vector<Double_t>> m_xGeometryOffset;
    std::vector<std::vector<Double_t>> m_yGeometryOffset;
    std::vector<std::vector<Double_t>> m_zGeometryOffset;

    std::vector<std::vector<Double_t>> m_angleGeometryOffset;
    std::vector<std::vector<Double_t>> m_tiltGeometryOffset;
    std::vector<std::vector<Double_t>> m_gammaGeometryOffset;

    Bool_t loadConfigFile(TString config_name, YAML::Node& config);

    ClassDef(MUonEDetectorGeometryMisalignmentsConfiguration, 2)

};


#endif //MUONEDETECTORGEOMETRYMISALIGNMENTSCONFIGURATION_H

