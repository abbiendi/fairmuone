#ifndef MUONEDETECTORMODULEDIGITIZATIONCONFIGURATION_H
#define MUONEDETECTORMODULEDIGITIZATIONCONFIGURATION_H

#include "Rtypes.h"

#include <string>
#include <unordered_map>
#include <yaml-cpp/yaml.h>


class MUonEDetectorModuleDigitizationConfiguration {

public:

    MUonEDetectorModuleDigitizationConfiguration();
    MUonEDetectorModuleDigitizationConfiguration(YAML::Node const& config);

    Bool_t readConfiguration(YAML::Node const& config);
    void updateConfiguration(YAML::Node const& config); //update for per-module settings
    void logCurrentConfiguration() const;


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    Int_t driftDirectionUpstream() const {return m_driftDirectionUpstream;}
    Int_t driftDirectionDownstream() const {return m_driftDirectionDownstream;}

    Double_t sigma0() const {return m_sigma0;}
    Double_t sigmaCoefficient() const {return m_sigmaCoefficient;}
    Double_t readoutNoise() const {return m_readoutNoise;}
    Double_t stripThreshold() const {return m_stripThreshold;}
    Int_t halfChannelStripOrder() const {return m_halfChannelStripOrder;}
    Int_t maxClusterWidth() const {return m_maxClusterWidth;}
    Double_t ptWidth() const {return m_ptWidth;}
    Double_t windowOffset() const {return m_windowOffset;}
    Int_t seedSensor() const {return m_seedSensor;}
    Bool_t isLUTEnabled() const {return m_enableLUT;}
    std::unordered_multimap<uint8_t, Double_t> LUTmap() const {return m_LUTmap;}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    Int_t m_driftDirectionUpstream{0};
    Int_t m_driftDirectionDownstream{0};

    Double_t m_sigma0{0};
    Double_t m_sigmaCoefficient{0};
    Double_t m_readoutNoise{0};
    Double_t m_stripThreshold{0};
    Int_t m_halfChannelStripOrder{0};
    Int_t m_maxClusterWidth{0};
    Double_t m_ptWidth{0};
    Double_t m_windowOffset{0};
    Int_t m_seedSensor{0};
    Bool_t m_enableLUT{0};
    std::unordered_multimap<uint8_t, Double_t> m_LUTmap{0};

    ClassDef(MUonEDetectorModuleDigitizationConfiguration, 2)
};

#endif //MUONEDETECTORMODULEDIGITIZATIONCONFIGURATION_H

