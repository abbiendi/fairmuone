#ifndef MUONEDETECTORMODULESCONFIGURATION_H
#define MUONEDETECTORMODULESCONFIGURATION_H

#include "Rtypes.h"

#include <string>
#include <yaml-cpp/yaml.h>
#include <stdexcept>

#include "MUonEDetectorModuleGeometryConfiguration.h"
#include "MUonEDetectorModuleDigitizationConfiguration.h"


class MUonEDetectorModulesConfiguration {

public:

    MUonEDetectorModulesConfiguration();
    MUonEDetectorModulesConfiguration(YAML::Node const& config);

    Bool_t readConfiguration(YAML::Node const& config);
    void updateConfiguration(YAML::Node const& config); //update for per-module settings
    void logCurrentConfiguration() const;


    Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

    MUonEDetectorModuleGeometryConfiguration const& geometry() const {
        if(m_hasGeometry)
            return m_geometry;
        else
            throw std::logic_error("Trying to access geometry from modulesConfiguration, but it's not defined in the detector configuration file.");
    }

    MUonEDetectorModuleDigitizationConfiguration const& digitization() const {
        if(m_hasDigitization)
            return m_digitization;
        else
            throw std::logic_error("Trying to access digitization from modulesConfiguration, but it's not defined in the detector configuration file.");
    }

    Bool_t hasDigitization() const {return m_hasDigitization;}
    Bool_t hasGeometry() const {return m_hasGeometry;}


    void resetDefaultConfiguration();

private:


    Bool_t m_configuredCorrectly{false};

    MUonEDetectorModuleGeometryConfiguration m_geometry;
    MUonEDetectorModuleDigitizationConfiguration m_digitization;

    Bool_t m_hasGeometry = false;
    Bool_t m_hasDigitization = false;

    ClassDef(MUonEDetectorModulesConfiguration, 2)
};

#endif //MUONEDETECTORMODULESCONFIGURATION_H

