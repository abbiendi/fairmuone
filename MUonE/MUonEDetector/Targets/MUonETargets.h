#ifndef MUONETARGETS_H
#define MUONETARGETS_H


#include "FairDetector.h"
#include "TLorentzVector.h"

#include <unordered_map>

#include "MUonEDetectorConfiguration.h"

class FairVolume;
class TClonesArray;

class MUonETargets : public FairDetector {

public:

    MUonETargets();
    MUonETargets(MUonEDetectorConfiguration const& detectorConfiguration, Int_t targetToBias = -1);

    virtual ~MUonETargets() override;

    void setConfiguration(MUonEDetectorConfiguration const& detectorConfiguration, Int_t targetToBias = -1);
    
    void setTargetToBias(Int_t targetToBias);

    //registers output containers
	virtual void   Register() override;

    //returns containers
	virtual TClonesArray* GetCollection(Int_t iColl) const override;

    //register detector geometry
    virtual void ConstructGeometry() override;

    //called at each step through the volume
    virtual Bool_t ProcessHits(FairVolume *v=0) override;

    //resets containers after each event
    virtual void   Reset() override;

    //cleanup
    virtual void EndOfEvent() override;

private:

    //detector information
    MUonEDetectorConfiguration m_detectorConfiguration;

    Int_t m_targetToBias{-1};

    //initialize materials
	Int_t InitMedium(const char* name);


	ClassDef(MUonETargets, 2)

};

#endif//MUONETARGETS_H 

