#ifndef MUONETRACKERPOINT_H
#define MUONETRACKERPOINT_H

/*
    Container storing information about the hits in the tracker.
*/

#include "MUonEDetectorPoint.h"
#include "MUonETrackerVolumeIDs.h"

class MUonETrackerPoint : public MUonEDetectorPoint {


public:

    MUonETrackerPoint() = default;

    MUonETrackerPoint(Int_t trackID, Int_t trackPDGCode, MUonETrackerVolumeIDs const& ids, 
        TVector3& enteringPositionGlobalCoordinates, TVector3& exitingPositionGlobalCoordinates,
        TVector3& enteringPositionLocalCoordinates, TVector3& exitingPositionLocalCoordinates,
        Double_t totalEnergyDeposit)
        : MUonEDetectorPoint(trackID, trackPDGCode,
        enteringPositionGlobalCoordinates, exitingPositionGlobalCoordinates,
        enteringPositionLocalCoordinates, exitingPositionLocalCoordinates,
        totalEnergyDeposit), 
        StationID(ids.stationID()), ModuleID(ids.moduleID()), SensorID(ids.sensorID())
        {}


    Int_t stationID() const {return StationID;}
    Int_t moduleID() const {return ModuleID;}
    Int_t sensorID() const {return SensorID;}

    Bool_t isUpstream() const {return SensorID == 0;}
    Bool_t isDownstream() const {return SensorID == 1;}

private:

    Int_t StationID{-1};
    Int_t ModuleID{-1};
    Int_t SensorID{-1};
    
	ClassDef(MUonETrackerPoint,2)
};

#endif //MUONETRACKERPOINT_H

