#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonECalorimeterPoint+;
#pragma link C++ class MUonEDetectorPoint+;
#pragma link C++ class MUonETrackerPoint+;
#pragma link C++ class MUonETrackerVolumeIDs+;

#endif