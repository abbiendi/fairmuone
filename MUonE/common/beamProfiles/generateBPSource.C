void generateBPSource() {

    //generate beam profile for generation from appropriate .dat file
    //note that due to the use of THn with 5 dimensions (for higher order correlations etc.)
    //the number of bins has to be reasonable, at least those that are filled (since we're using THnSparse)


    #include <TFile.h>
    #include <TH1D.h>
    #include <THnSparse.h>
    
    #include <fstream>
    #include <iostream>



    std::string sourcefile = "output_mue_mu160.dat";
    std::string outputfile = "beamProfileTR.root";
    bool includeHaloMuons = true;

    std::ifstream infile;
    infile.open(sourcefile);

    TFile* outfile = new TFile(outputfile.c_str(), "RECREATE");

    TH1D* h_x = new TH1D("X"," ", 1000,-300,300);
    TH1D* h_y = new TH1D("Y"," ", 1000,-300,300);
    TH1D* h_xp = new TH1D("Xp"," ", 1000,-5,5);
    TH1D* h_yp = new TH1D("Yp"," ", 1000,-5,5); 
    TH1D* h_p = new TH1D("P"," ", 1000,0,200);   

    Int_t bins[5] = {1000, 1000, 1000, 1000, 1000};
    Double_t xmin[5] = {-300, -300, -5, -5, 0};
    Double_t xmax[5] = {300, 300, 5, 5, 200};
    THnSparseD* h_combined = new THnSparseD("combined", "", 5, bins, xmin, xmax);

    std::string line;

    while(getline(infile, line)) {

        std::stringstream s(line);

        int step{-9999};
        int particle{-9999};
        Double_t x{-9999};
        Double_t y{-9999};
        Double_t z{-9999};
        Double_t xp{-9999};
        Double_t yp{-9999};
        Double_t p{-9999};

        s >> step >> particle >> x >> y >> z >> xp >> yp >> p;
    
        Double_t combined[5] = {x,y,xp,yp,p};

        if(1079 < z && z < 1080) {

            if(includeHaloMuons) {

                h_x->Fill(x);
                h_y->Fill(y);
                h_xp->Fill(xp);
                h_yp->Fill(yp);
                h_p->Fill(p);
                h_combined->Fill(combined);

            } else {

                if(2 == particle) {

                    h_x->Fill(x);
                    h_y->Fill(y);
                    h_xp->Fill(xp);
                    h_yp->Fill(yp);
                    h_p->Fill(p);     
                    h_combined->Fill(combined);               
                }
            }

        }

    
    }

    /*h_x->Write();
    h_y->Write();
    h_xp->Write();
    h_yp->Write();
    h_p->Write();
*/
    h_combined->Write();
    outfile->Write();
    outfile->Close();

}