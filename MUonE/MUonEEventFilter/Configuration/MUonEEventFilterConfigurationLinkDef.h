#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class MUonEEventFilterConfiguration+;
#pragma link C++ class MUonEGeneratedEventsFilterConfiguration+;
#pragma link C++ class MUonEDigitizedEventsFilterConfiguration+;
#pragma link C++ class MUonEReconstructedEventsFilterConfiguration+;


#endif