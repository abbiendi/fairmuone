#include "MUonEGeneratedEventsFilterConfiguration.h"

#include <fairlogger/Logger.h>


MUonEGeneratedEventsFilterConfiguration::MUonEGeneratedEventsFilterConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonEGeneratedEventsFilterConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["minNumberOfMonteCarloTracks"]) {

        m_isActive = true;
        m_minNumberOfMonteCarloTracksActive = true;
        m_minNumberOfMonteCarloTracks = config["minNumberOfMonteCarloTracks"].as<Int_t>();
    }

    if(!m_configuredCorrectly)
        LOG(info) << "Generated events filter configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEGeneratedEventsFilterConfiguration::logCurrentConfiguration() const {
    
    if(m_isActive) {
        LOG(info) << "Generated events filter is active.";

        if(m_minNumberOfMonteCarloTracksActive)
            LOG(info) << "minNumberOfMonteCarloTracks: " << m_minNumberOfMonteCarloTracks;
    }
}

void MUonEGeneratedEventsFilterConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_isActive = false;

    m_minNumberOfMonteCarloTracksActive = false;
    m_minNumberOfMonteCarloTracks = 0;
 
}

ClassImp(MUonEGeneratedEventsFilterConfiguration)