import argparse

parser = argparse.ArgumentParser()

parser.add_argument("workpath", type=str)

parser.add_argument("-n", "--numberOfEvents", default=-1, type=int)
parser.add_argument("-dc", "--detectorConfiguration", default="TR2023_geometry_3cmTarget_noECAL", type=str)
parser.add_argument("-abc", "--addBaselineMSCorrectionToAllTracks", default="true", type=str)
parser.add_argument("-retwmsc", "--refitElectronTrackWithMSCorrection", default="true", type=str)
parser.add_argument("-rmtwmsc", "--refitMuonTrackWithMSCorrection", default="true", type=str)
parser.add_argument("-ritwmsc", "--refitIncomingTrackWithMSCorrection", default="true", type=str)
parser.add_argument("-ufzpikf", "--useFittedZPositionInKinematicFit", default="true", type=str)
parser.add_argument("-rvptt", "--restrictVertexPositionToTarget", default="true", type=str)
parser.add_argument("-ufpikf", "--useFittedPositionInKinematicFit", default="false", type=str)
parser.add_argument("-rhstfnmis", "--restrictHitSharingToFirstNModulesInSector", default=2, type=int)
parser.add_argument("-moc2", "--maxOutlierChi2", default=-1, type=float)

parser.add_argument("-dndcmt", "--disableNonDiagonalCovarianceMatrixTerms", default="false", type=str)
parser.add_argument("-vsat", "--vertexSplittingAngleThreshold", default=-1, type=float)
parser.add_argument("-rhakf", "--reassignHitsAfterKinematicFit", default="false", type=str)
parser.add_argument("-amscftikf", "--addMSCorrectionFromTargetInKinematicFit", default="true", type=str)




args = parser.parse_args()
print("Generating production file, arguments used:")
print(args)

theta_ranges = [
    (0, 5),
    (5, 10),
    (10, 15),
    (15, 20),
    (20, 25),
    (25, 32)
]

hits_shared = [0, 1, 2]

for lower_theta, upper_theta in theta_ranges:

    for shared in hits_shared:
    
        config = """

outputFile: {0}.root
detectorConfiguration: {1}

numberOfEvents: {2}

inputDirectory: {3}/
inputFiles: ['{4}']

runGeneration: false
runDigitization: false
runEventFilter: false 



runReconstruction: true
reconstructionConfiguration:

    isMC: true
    verbose: false
    runAdaptiveVertexFitter: false

    outputFormat: full



    addBaselineMSCorrectionToAllTracks: {5}
    
    refitElectronTrackWithMSCorrection: {6}
    refitMuonTrackWithMSCorrection: {7}
    refitIncomingTrackWithMSCorrection: {8}

    useFittedZPositionInKinematicFit: {9}
    restrictVertexPositionToTarget: {10}
    useFittedPositionInKinematicFit: {11}


    restrictHitSharingToFirstNModulesInSector: {12}
    maxOutlierChi2: {13}

    maxNumberOfSharedHits: {14}

    disableNonDiagonalCovarianceMatrixTerms: {15}
    vertexSplittingAngleThreshold: {16}
    reassignHitsAfterKinematicFit: {17}

    addMSCorrectionFromTargetInKinematicFit: {18}


        """.format(
            
                "{0}/reconstructed_{1}_{2}_{3}".format(args.workpath, lower_theta, upper_theta, shared), 
                args.detectorConfiguration,
                args.numberOfEvents,
                args.workpath,
                "generated_{0}_{1}.root".format(lower_theta, upper_theta),
                args.addBaselineMSCorrectionToAllTracks,
                args.refitElectronTrackWithMSCorrection,
                args.refitMuonTrackWithMSCorrection,
                args.refitIncomingTrackWithMSCorrection,
                args.useFittedZPositionInKinematicFit,
                args.restrictVertexPositionToTarget,
                args.useFittedPositionInKinematicFit,
                args.restrictHitSharingToFirstNModulesInSector,
                args.maxOutlierChi2,
                shared,
                args.disableNonDiagonalCovarianceMatrixTerms,
                args.vertexSplittingAngleThreshold,
                args.reassignHitsAfterKinematicFit,
                args.addMSCorrectionFromTargetInKinematicFit
            )


        with open("{0}/reconstruct_{1}_{2}_{3}.yaml".format(args.workpath, lower_theta, upper_theta, shared), 'w') as f:
            f.write(config)