#!/bin/bash

cd $FAIRMUONEPATH/share/MUonE/macros/

for file in ${1}/reconstruct_*.yaml
do
    root -q -l runProductionJob.C'("'${file}'")'
done