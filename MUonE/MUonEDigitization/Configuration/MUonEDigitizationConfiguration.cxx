#include "MUonEDigitizationConfiguration.h"


#include <fairlogger/Logger.h>


MUonEDigitizationConfiguration::MUonEDigitizationConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonEDigitizationConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["tracker"]) {

        if(!m_tracker.readConfiguration(config["tracker"]))
            m_configuredCorrectly = false;

    } else {

        LOG(info) << "digitizationConfiguration/tracker section not found. Default configuration will be used";
    }

    if(config["calorimeter"]) {

        if(!m_calorimeter.readConfiguration(config["calorimeter"]))
            m_configuredCorrectly = false;

    } else {

        LOG(info) << "digitizationConfiguration/calorimeter section not found. Default configuration will be used";
    }    


    if(!m_configuredCorrectly)
        LOG(info) << "Digitization configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonEDigitizationConfiguration::logCurrentConfiguration() const {
    
    LOG(info) << "";
	LOG(info) << "Digitization configuration:";

    m_tracker.logCurrentConfiguration();
    m_calorimeter.logCurrentConfiguration();
}

void MUonEDigitizationConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_tracker.resetDefaultConfiguration();
    m_calorimeter.resetDefaultConfiguration();
}

ClassImp(MUonEDigitizationConfiguration)


