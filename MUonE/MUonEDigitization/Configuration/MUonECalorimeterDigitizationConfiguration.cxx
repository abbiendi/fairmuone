#include "MUonECalorimeterDigitizationConfiguration.h"


#include <fairlogger/Logger.h>


MUonECalorimeterDigitizationConfiguration::MUonECalorimeterDigitizationConfiguration() {

    resetDefaultConfiguration();
}

Bool_t MUonECalorimeterDigitizationConfiguration::readConfiguration(YAML::Node const& config) {

    resetDefaultConfiguration();
    m_configuredCorrectly = true;

    if(config["saveDeposit"])
        m_saveDeposit = config["saveDeposit"].as<Bool_t>();
    else {
        LOG(info) << "Variable 'saveDeposit' not set. It will be saved by default.";
        m_saveDeposit = true;            
    }

    if(!m_configuredCorrectly)
        LOG(info) << "Calorimeter digitization configuration contains errors. Please fix them and try again.";


    return m_configuredCorrectly;
}

void MUonECalorimeterDigitizationConfiguration::logCurrentConfiguration() const {
    
    LOG(info) << "";
	LOG(info) << "Calorimeter digitization configuration:";
    if(m_saveDeposit)
        LOG(info) << "Deposit will be saved.";
    else
        LOG(info) << "Deposit will not be saved."; 

}

void MUonECalorimeterDigitizationConfiguration::resetDefaultConfiguration() {

    m_configuredCorrectly = false;

    m_saveDeposit = true;
}

ClassImp(MUonECalorimeterDigitizationConfiguration)

