#ifndef MUONESIGNALPOINT_H
#define MUONESIGNALPOINT_H

#include "TVector2.h"

class MUonESignalPoint {

public:

    MUonESignalPoint() = default;

    MUonESignalPoint(TVector2 const& position, Double_t sigma, Double_t amplitude)
        : m_position(position), m_sigma(sigma), m_amplitude(amplitude)
        {}


    TVector2 const& position() const {return m_position;}
    Double_t sigma() const {return m_sigma;}
    Double_t amplitude() const {return m_amplitude;}


private:


    TVector2 m_position;

    Double_t m_sigma{0}; //assumed to be the same in both x and y direction, cm
    Double_t m_amplitude{0};

    ClassDef(MUonESignalPoint,2)

};

#endif //MUONESIGNALPOINT_H

