#ifndef MUONETRACKERDIGITIZATION_H
#define MUONETRACKERDIGITIZATION_H

#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>

#include "MUonEDetectorConfiguration.h"
#include "MUonETrackerDigitizationConfiguration.h"

#include "MUonETrackerPoint.h"
#include "MUonEChargeDeposit.h"
#include "MUonESignalPoint.h"
#include "MUonETrackerStripDigi.h"
#include "MUonETrackerCluster.h"
#include "MUonETrackerStub.h"

#include "FairTask.h"
#include "Rtypes.h"
#include "TClonesArray.h"


/*

   The stub digitization algorithm originaly developed
   by Riccardo Pilato (https://gitlab.cern.ch/muesli/digitiser/-/tree/rpilato) 

*/


//defined for convenience


//maps for digitization

//key: stripNo. value: stripDeposit (see MUonETrackerStripDigi.h)
typedef std::unordered_map<Int_t, MUonEStripDeposits>       StripsMap;

//key: theSensorHalf.
typedef std::unordered_map<Int_t, StripsMap>      SensorHalfsMap;

//key: sensorId.
typedef std::unordered_map<Int_t, SensorHalfsMap> SensorsMap;

//key: ModuleID.
typedef std::unordered_map<Int_t, SensorsMap>     ModulesMap;

//key: stationID.
typedef std::unordered_map<Int_t, ModulesMap>     DigisMap;



//maps for stub creation
//key: stripId
struct compareStripDigis{
	bool operator()(const MUonETrackerStripDigi* lhs, const MUonETrackerStripDigi* rhs) const
	{
		return lhs->stripID() < rhs->stripID();
	}
};
typedef std::set<const MUonETrackerStripDigi*, compareStripDigis>     HitSet; //the set is sorted by stripID
	
//key: sensorId
typedef std::unordered_map<Int_t, HitSet>                   SensorHitMap;

//key: sensorHalf
typedef std::unordered_map<Int_t, SensorHitMap>             SensorHalfsHitMap;

//key: moduleID
typedef std::unordered_map<Int_t, SensorHalfsHitMap>        ModulesHitMap;

//key: stationID
typedef std::unordered_map<Int_t, ModulesHitMap>            HitMap;



typedef std::set<MUonETrackerCluster>           ClusterSet;
//key: sensorID
typedef std::unordered_map<Int_t, ClusterSet>   ClusterSets;



typedef std::set<MUonETrackerStub>              StubSet;
//key: cbcId
typedef std::unordered_map<Int_t, StubSet>      StubSets;


class MUonETrackerDigitization : public FairTask {


public:

    MUonETrackerDigitization();
    virtual ~MUonETrackerDigitization();

	Bool_t setConfiguration(MUonEDetectorConfiguration const& detectorConfig, MUonETrackerDigitizationConfiguration const& digitizationConfig);

	Bool_t configuredCorrectly() const {return m_configuredCorrectly;}

	void logCurrentConfiguration();
	void resetDefaultConfiguration();

    virtual InitStatus ReInit();
    virtual InitStatus Init();
	void Register();

    virtual void Exec(Option_t* option);
    virtual void Finish();   


private:

	Bool_t m_configuredCorrectly{false};

    MUonEDetectorConfiguration m_detectorConfiguration;
    MUonETrackerDigitizationConfiguration m_digitizationConfiguration;

    const TClonesArray* m_trackerPoints{nullptr};

    TClonesArray* m_trackerStripDigis{nullptr};
    TClonesArray* m_trackerStubs{nullptr};


    const Double_t GEV_PER_ELECTRON = 3.61e-9; //GeV, energy required for a single electron-hole pair production in Silicon (primary_ionisation)


    void primaryIonisation(const MUonETrackerPoint* point, std::vector<MUonEChargeDeposit>& ionisation_points);
    void drift(std::vector<MUonEChargeDeposit> const& ionisation_points, std::vector<MUonESignalPoint>& collection_points, const MUonETrackerPoint* point);
    void induceSignal(std::vector<MUonESignalPoint> const& collection_points, DigisMap& digis_map, const MUonETrackerPoint* point);
    void createDigis(DigisMap const& digis_map);

    void createStubs();

    MUonETrackerStripDigi* AddStripDigi(Int_t station, Int_t module, Int_t sensor, Int_t sensorHalf, Int_t strip, Double_t charge, MUonEStripDeposits& deposits);
    MUonETrackerStub* AddStub(MUonETrackerStub const& stub);
    MUonETrackerStub ApplyLUT(MUonETrackerStub const& stub);
    MUonETrackerStub CorrectBendOffset(MUonETrackerStub const& stub);

    ClassDef(MUonETrackerDigitization,2)
};

#endif //MUONETRACKERDIGITIZATION_H



