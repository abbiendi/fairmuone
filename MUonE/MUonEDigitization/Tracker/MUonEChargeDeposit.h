#ifndef MUONECHARGEDEPOSIT_H
#define MUONECHARGEDEPOSIT_H

#include "TVector3.h"


class MUonEChargeDeposit {

public:

    MUonEChargeDeposit() = default;
    MUonEChargeDeposit(Double_t charge, TVector3 const& position)
        : m_charge(charge), m_position(position)
        {}


    Double_t charge() const {return m_charge;}
    const TVector3& position() const {return m_position;}
    

private:

    Double_t m_charge{0};
    TVector3 m_position;


    ClassDef(MUonEChargeDeposit,2)
};

#endif //MUONECHARGEDEPOSIT_H

