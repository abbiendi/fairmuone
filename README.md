# FairMUonE

Software for Monte Carlo production and event reconstruction for the MUonE experiment.

For build instructions see "SETUP" and "SETUP ON LXPLUS" sections. For information about dockers and their use locally/on lxplus see "DOCKERS" section. For basic usage instructions see "USAGE".
The settings available in configuration files are listed in the "List of available settings" document in the repository.

## SETUP

1. Clone, build and install FairSoft (please refer to the installation instructions on the project's github page; note that it has dependencies you have to install first):

	`https://github.com/FairRootGroup/FairSoft/blob/master/legacy/README.md`

2. Clone FairRoot repository.

	`https://github.com/FairRootGroup/FairRoot`

3. Clone this repository.

4. Place MUonE directory from this repository inside your cloned FairRoot directory, in the fairroot subdirectory (i.e. FairRoot/fairroot).

5. Edit FairRoot/fairroot/CMakeLists.txt and add line:

	`add_subdirectory(MUonE)`


6. Follow instructions in FairRoot repository to compile and install FairRoot (only the install FairRoot part, ignore the latter part about project templates).

	`https://github.com/FairRootGroup/FairRoot?tab=readme-ov-file#step-by-step-installation`

7. FairMUonE will be installed to the directory you set with `-DCMAKE_INSTALL_PREFIX` in the previous step.

## SETUP ON LXPLUS

Warning -- the method below relies on using FairSoft packages distributed via cvmfs (it can't be easily built on lxplus), which are often older than releases available from FairSoft repository. Unless you specifically need to build FairMUonE on lxplus manually, use one of the available docker containers instead (see "DOCKERS" section).

Make sure to login on CentOS8 machine ("@lxplus8", using CentOS7 will result in missing dependencies errors). Instead of building FairSoft manually, set SIMPATH to `/cvmfs/fairsoft.gsi.de/centos8/fairsoft/<version you want to use>`, then follow setup instructions starting from point 2.

WARNING

Since v18.8.0, FairRoot is set to force C++17 standard, which does not work smoothly with the default version of GCC compiler (8) and centos8 is not really officially supported by FS and FR as far as I can tell.
You may encounter these problems (thanks Eugenia):
1) When running

``` cmake -DCMAKE_INSTALL_PREFIX=~/fair_install/FairRootInst" .. ```

error about FairRoot tutorial may appear:

```

CMake Error at examples/simulation/Tutorial4/src/CMakeLists.txt:38 (add_library):

  Target "ExSimulation4" links to target "FairRoot::EventDisplay" but the

  target was not found.  Perhaps a find_package() call is missing for an

  IMPORTED target, or an ALIAS target is missing?

```

To fix it, remove the directory of tutorial number 4 in ```examples/simulation/```. This applies to any errors related to tutorials, as we're not using them.

 
2) When running

``` make ```

you may get an error about linking to std::filesystem (similar message repeated multiple times):

 
```
[ 31%] Linking CXX shared library ../../lib/libMUonEProductionJob.so
CMakeFiles/MUonEProductionJob.dir/MUonEProductionJob.cxx.o: In function `MUonEProductionJob::readConfiguration(TString)':
/usr/include/c++/8/bits/fs_path.h:185: undefined reference to `std::filesystem::__cxx11::path::_M_split_cmpts()'

```

add ```link_libraries(stdc++fs)```  to ```FairRoot/CMakeLists.txt``` right after line 14.
Unfortunately, this fix seems to be specific to the combination of C++17 standard and GCC8 compiler and fixing it centrally would be problematic (filesystem library has an experimental implementation pre-GCC8 and is part of the standard and linked automatically post-GCC8). The problem should also be fixed by switching to a higher version of the compiler.

## DOCKERS

FairMUonE is distributed also as docker containers, the list of which can be found in

`https://gitlab.cern.ch/muesli/offline-sw/fairmuone_dockers/container_registry/21578`

additional information (e.g. about preinstalled packages) can be found in the same repository's readme:

`https://gitlab.cern.ch/muesli/offline-sw/fairmuone_dockers/`

The dockers are labelled with the versions of FairSoft, FairRoot, FairMUonE and Mesmer. The link to a particular docker can be copied by clicking the icon to the right of its name and should look similar to this:

`gitlab-registry.cern.ch/muesli/offline-sw/fairmuone_dockers:fairmuone_FS_jan24p3_FR_v19.0.0_FM_v0.14.6_MSMR_v1.2.0-dev` 


To run the dockers I suggest using Singularity. It is readily available on lxplus and can be installed locally using instructions in (make sure to check the section about distribution packages as it can be installed with apt/yum on most distributions, but the instructions start with a section about building it from source):

`https://docs.sylabs.io/guides/3.0/user-guide/installation.html`

If you are using lxplus, make sure to change the directory for temporary cache files or you will run out of quota. You can set it, for example, to /tmp:

`export SINGULARITY_CACHEDIR="/tmp/$(whoami)/singularity"`

To download one of the docker images, run:

`singularity pull --docker-login docker://<link to image you want to pull>`

where the link is the one copied previously (also note the "docker://" part in front, it has to be there). For example:

`singularity pull --docker-login docker://gitlab-registry.cern.ch/muesli/offline-sw/fairmuone_dockers:fairmuone_FS_jan24p3_FR_v19.0.0_FM_v0.14.6_MSMR_v1.2.0-dev`

You will have to login with your cern gitlab credentials to do so.

Running the above will create an image (.sif extention) file in your current directory. To run it, execute:

`singularity shell <the image>`

For example:

`singularity shell fairmuone_FS_jan24p3_FR_v19.0.0_FM_v0.14.6_MSMR_v1.2.0-dev.sif`

The software will be under /fairmuone and is exactly like a normal FairMUonE installation.

Note that:
1) Your host (i.e. lxplus) home directory will be automatically mounted as ~/ (and it's not the same as /home in the docker)
2) If you want to mount additional paths, you can do so with -B or --bind (they're equivalent). See `https://singularity-tutorial.github.io/05-bind-mounts/` for examples.
3) If you want to be able to write in the docker container, you need to add --writable-tmpfs after shell. If you don't and you run an example production job inside of the docker, you will get a segmentation fault when it tries to create the output ROOT file.

## USAGE

1. Run FairRootConfig.sh (or .csh, depending on shell) script from bin subdirectory in FairMUonE installation directory.

	`source FairRootConfig.sh`

2. Enter share/MUonE/macros.

3. To run example production job, run `root -l 'runProductionJob.C("exampleProductionJob")'`

4. To run event display, run `root -l 'runEventDisplay.C("exampleProductionJob.root")'`

5. To run analysis job, run `root -l 'runAnalysisJob.C("exampleAnalysisJob")'`

The configuration files are stored in share/MUonE/common/
1) alignment for alignment corrections
2) beamProfiles for profiles to be used with beam generators (they can also be viewed by opening the corresponding ROOT file, e.g. with TBrowser)
3) gconfig for the Geant4 options
4) geometry for detector and material configurations
5) jobs for job definitions

All configuration files are written in YAML language. For the list of available options see the "List of available settings" document in the repository.


## CURRENTLY KNOWN PROBLEMS AND OTHER INFORMATION

Event display works only with detector geometry, as the macro has to be adapted to custom track and hit classes.
